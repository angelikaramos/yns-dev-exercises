<!DOCTYPE html>
<html>
    <head>
        <title>Input characters in text box and show it in label.</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
        <script>
            window.addEventListener('DOMContentLoaded', (event) => {
                // Getting html elements by id
                var input = document.getElementById('input');
                var result = document.getElementById('result');

                input.addEventListener('input', updateValue);

                // Passing the value typed in the result variable
                function updateValue(e) {
                    result.textContent = "You're typing: " + e.target.value;
                }
            });
        </script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include 'includes/navbar.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                </div>
                <!-- Right side form -->
                <div class="col-lg-6 col-md-6 form-container">
                    <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
                        <!-- Logo -->
                        <div class="logo mb-3">
                            <img src="assets/img/yns.png" alt="yns_logo" width="150px">
                        </div>
                        <!-- Heading or Title of Exercise -->
                        <div class="heading mb-4">
                            <h4>Input in Text Box and show it in Label</h4>
                        </div>
                        <!-- Form -->
                        <div class="form-input">
                            <label for="type_here">Type Here</label>
                            <input id="input"/><br><br>
                        </div>
                        <label id="result"></label>               
                    </div>
                </div>
            </div>
        </div> 
    </body>
</html>