<!DOCTYPE html>
<html>
    <head>
        <title>Show images according to the options in combo box.</title>
                <!-- CSS only -->
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
        <script>
            window.addEventListener('DOMContentLoaded', (event) => {
                // Get html element by id
                var cartoonList = document.getElementById('cartoons');
                var img = document.getElementById('img')
                
                // When select other options 
                cartoonList.addEventListener('change', (event) => {
                    // Get selected option
                    var selected = cartoonList.selectedOptions;
                    for (var i = 0; i < selected.length; i++) {
                        // Assign value to image src
                        img.src = selected[i].value;
                    }
                });
            });
        </script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include 'includes/navbar.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                </div>
                <!-- Right side form -->
                <div class="col-lg-6 col-md-6 form-container">
                    <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
                        <!-- Logo -->
                        <div class="logo mb-3">
                            <img src="assets/img/yns.png" alt="yns_logo" width="150px">
                        </div>
                        <!-- Heading or Title of Exercise -->
                        <div class="heading mb-4">
                            <h4>Images according to the Options</h4>
                        </div>
                        <!-- Images -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-input">
                                    <img src="assets/img/kim_possible.jpg" id="img" style="height:200%;width:70%"><br><br>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-input">
                                    <select name="cartoons" id="cartoons">
                                        <option value="assets/img/kim_possible.jpg" selected>Kim Possible</option>
                                        <option value="assets/img/LooneyTunes.jpg">Looney Tunes</option>
                                        <option value="assets/img/Phineas_and_Ferb.jpg">Phineas and Ferb</option>
                                        <option value="assets/img/ShaunTheSheep.jpg">Shaun the Sheep</option>
                                        <option value="assets/img/TheAmazingWorldOfGumball.jpg">The Amazing World of Gumball</option>
                                        <option value="assets/img/TheFairlyOddParents.jpg">The Fairly Odd Parents</option>
                                    </select>
                                </div>
                            </div>
                        </div>        
                    </div>
                </div>
            </div>
        </div> 
    </body>
</html>