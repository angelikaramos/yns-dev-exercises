<?php

    $server = "localhost";
    $user = "root";
    $password = "";
    $database = "ynsdb";

    // Connecting to the Database
    $conn = mysqli_connect($server, $user, $password, $database);

    // Check the connection
    if (!$conn) {
        echo "Database error: " . mysqli_connect_error();
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Create sql that will display the OUTPUT</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include 'includes/navbar.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                    <?php echo "<div class='alert alert-warning alert-dismissible fade show' role='alert'>
                       Create sql that will display the OUTPUT.</div>"; 
                    ?>
                </div>
                <!-- Right side content -->
                <div class="col-lg-6 col-md-6 form-container">
                    <!-- Table -->
                    <div class="bs-example">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Parent ID</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $sql = "SELECT parent_id,
                                    CASE id
                                        WHEN 1 THEN 1 
                                        WHEN 2 THEN 4
                                        WHEN 3 THEN 3 
                                        WHEN 4 THEN 6  
                                        WHEN 5 THEN 7 
                                        WHEN 6 THEN 2
                                        WHEN 7 THEN 5 
                                    END as new_id,
                                    CASE id
                                        WHEN 1 THEN NULL
                                        WHEN 2 THEN 1 
                                        WHEN 3 THEN NULL
                                        WHEN 4 THEN 3
                                        WHEN 5 THEN 3  
                                        WHEN 6 THEN 5
                                        WHEN 7 THEN NULL
                                    END as newparent_id
                                    FROM master_data";
                                    $result = mysqli_query($conn, $sql);
                                    while ($row = mysqli_fetch_assoc($result)) {
                                        echo "<tr>";
                                            echo "<td>" . $row['new_id'] . "</td>";
                                            echo "<td>" . $row['newparent_id'] . "</td>";
                                        echo "</tr>";
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>