<?php

    // Data from form
    if (isset($_POST['first_number']) && isset($_POST['second_number'])) {
        $first_number = $_POST['first_number'];
        $second_number = $_POST['second_number'];
    }
    if (isset($_POST['add'])) { 
        $add = $_POST['add'];    
    }
    if (isset($_POST['subtract'])) {
        $subtract = $_POST['subtract'];
    }
    if (isset($_POST['multiply'])) {
        $multiply = $_POST['multiply'];
    }
    if (isset($_POST['divide'])) {
        $divide = $_POST['divide'];
    }

    class FourBasicOperations {

        private $first_number;
        private $second_number;

        public function addition($fnumber,$snumber) {
            $result = $this->first_number = $fnumber + $this->second_number = $snumber;

            return $result;
        }

        public function subtraction($fnumber,$snumber) {
            $result = $this->first_number = $fnumber - $this->second_number = $snumber;

            return $result;
        }

        public function multiplication($fnumber,$snumber) {
            $result = $this->first_number = $fnumber * $this->second_number = $snumber;

            return $result;
        }

        public function division($fnumber,$snumber) {
            $result = $this->first_number = $fnumber / $this->second_number = $snumber;

            return $result;
        }
    }

    $operation = new FourBasicOperations();

    // Button operation clicked    
    if (isset($add)) {
        $result = $operation->addition($first_number,$second_number);
    }
    if (isset($subtract)) {
        $result = $operation->subtraction($first_number,$second_number);
    }
    if (isset($multiply)) {
        $result = $operation->multiplication($first_number,$second_number);
    }
    if (isset($divide)) {
        $result = $operation->division($first_number,$second_number);
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>The Four Basic Operations of Arithmetic.</title> 
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include 'includes/navbar.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                </div>
                <!-- Right side form -->
                <div class="col-lg-6 col-md-6 form-container">
                    <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
                        <!-- Logo -->
                        <div class="logo mb-3">
                            <img src="assets/img/yns.png" alt="yns_logo" width="150px">
                        </div>
                        <!-- Heading or Title of Exercise -->
                        <div class="heading mb-4">
                            <h4>Arithmetic Operations</h4>
                        </div>
                        <!-- Form -->
                        <form method="post" action="1-2.php">
                            <div class="form-input">
                                <label for="first_number">First Number:</label><br>
                                <input type="text" name="first_number" required> <br><br>
                            </div>
                            <div class="form-input">
                                <label for="second_number">Second Number:</label><br>
                                <input type="text" name="second_number" required> <br><br>
                            </div>
                                <label for="result">Result :
                                    <?php 
                                        if (isset($result)) {
                                            echo $result; 
                                        }
                                    ?>
                                </label><br><br>         
                            <button type="submit" name="add">Addition</button>
                            <button type="submit" name="subtract">Subtraction</button>
                            <button type="submit" name="multiply">Multiplication</button>
                            <button type="submit" name="divide">Division</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>