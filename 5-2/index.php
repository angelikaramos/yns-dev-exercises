<?php
    // Set timezone
    date_default_timezone_set('Asia/Manila');

    // Get prev and & next month
    if (isset($_GET['yearMonth'])) {
        $yearMonth = $_GET['yearMonth'];
    }
    if (empty($_GET['yearMonth'])) {
        // This month of this year
        $yearMonth = date('Y-m');
    }

    // Check format
    $timestamp = strtotime($yearMonth,"-01");
    if ($timestamp === false) {
        $timestamp = time();
    }

    // Today 
    $today = date('Y-m-d', time());

    // Month Title
    $monthTitle = date('F Y', $timestamp);

    // Prev and Next Month link  mktime (hour, minute, second, month, day, year)
    $prev = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)-1, 1, date('Y', $timestamp)));
    $next = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)+1, 1, date('Y', $timestamp)));

    // Number of days in month
    $countDays  = date('t', $timestamp);

    // 0:Sun 1:Mon 2:Tue ... w - numeric representation of the day
    $str = date('w', mktime(0, 0, 0, date('m', $timestamp), 1, date('Y', $timestamp)));

    // Calendar
    $weeks = array();
    $week = '';
    
    // Empty cell
    $week .= str_repeat("<td></td>", $str);

    for ($day = 1; $day <= $countDays; $day++, $str++) {
        $date = $yearMonth . "-" . $day;

        if ($today == $date) {
            $week .= "<td class='today'>" . $day;
        }
        if ($today != $date) {
            $week .= "<td>" . $day;
        }
        $week .= "</td>";

        // End of the week or end of the month
        if ($str % 7 == 6 || $day == $countDays) {
            if ($day == $countDays) {
                // Empty Cell
                $week .= str_repeat("<td></td>", 6 - ($str % 7));
            }
            $weeks[] = "<tr>" . $week . "</tr>";

            // For new week
            $week = "";
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Calendar</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
    </head>
    <body>
        <div class="container">
            <!-- Month and Year -->
            <strong><p class="display-4"><?php echo $monthTitle; ?></p></strong>
            <!-- Previous Button -->
            <a href="?yearMonth=<?php echo $prev; ?>"><button class="btn btn-primary">Prev</button></a>
            <!-- Today Button -->
            <a  href="index.php"><button class="btn btn-success">Today</button></a>
            <!-- Next Button -->
            <a  href="?yearMonth=<?php echo $next; ?>"><button class="btn btn-primary">Next</button></a><br>
            <table class="table table-bordered">
                <tr class="table-info">
                    <th>S</th>
                    <th>M</th>
                    <th>T</th>
                    <th>W</th>
                    <th>T</th>
                    <th>F</th>
                    <th>S</th>
                </tr>
                <?php
                    foreach($weeks as $week) {
                        echo $week;
                    }
                ?>
            </table>
        </div>
    </body>
</html>