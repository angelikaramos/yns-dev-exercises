<!DOCTYPE html>
<html>
    <head>
        <title>Press button and add a label below button.</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
        <script>
            window.addEventListener('DOMContentLoaded', (event) => {
                // Get html element by id
                var btn = document.getElementById('btn');
                var label = document.getElementById('label');  

                // When button clicked, adding label function
                btn.addEventListener('click', function() {
                    // Create label and its text
                    var newlabel = document.createElement('LABEL');
                    var text = document.createTextNode('This is a new label');
                    
                    // Append text to label
                    newlabel.appendChild(text);

                    // Display label tag,text and br tag
                    label.appendChild(newlabel);
                    label.appendChild(document.createElement('BR'));
                });
            });
        </script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include 'includes/navbar.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                </div>
                <!-- Right side form -->
                <div class="col-lg-6 col-md-6 form-container">
                    <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
                        <!-- Logo -->
                        <div class="logo mb-3">
                            <img src="assets/img/yns.png" alt="yns_logo" width="150px">
                        </div>
                        <!-- Heading or Title of Exercise -->
                        <div class="heading mb-4">
                            <h4>Press button and Add label</h4>
                        </div>
                        <!-- Button and label -->
                        <button type="submit" id="btn">Add label</button>
                        <label for="label" id="label"></label>           
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>