<?php

    if (isset($_POST['Submit'])) {
        // Data from form
        $number = $_POST['number'];
        
        // Initialize variable
        $results = array();

        for($i = 1; $i <= $number; $i++) {
            if ($i % 3 == 0 && $i % 5 == 0) {
                $result = "FizzBuzz";
            } elseif ($i % 3 == 0) {
                $result = "Fizz";
            } elseif ($i % 5 == 0) {
                $result = "Buzz";
            } else {
               $result = $i;
            }
            array_push($results, $result);
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Solve FizzBuzz problem</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include 'includes/navbar.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                </div>
                <!-- Right side form -->
                <div class="col-lg-6 col-md-6 form-container">
                    <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
                        <!-- Logo -->
                        <div class="logo mb-3">
                            <img src="assets/img/yns.png" alt="yns_logo" width="150px">
                        </div>
                        <!-- Heading or Title of Exercise -->
                        <div class="heading mb-4">
                            <h4>FizzBuzz Problem</h4>
                        </div>
                        <!-- Form -->
                        <form method="post" action="1-4.php">
                            <div class="form-input">
                                <label for="number">Enter a number:</label><br>
                                <input type="text" name="number" required><br><br>
                            </div>
                            <button type="submit" name="Submit">Submit</button><br><br>
                            <label for="result">Result :<br>
                                <?php
                                    if (isset($results)) {
                                        foreach($results as $result) {
                                            echo $result . " ";
                                        }
                                    }
                                ?>
                            </label>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>