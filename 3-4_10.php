<?php

    $server = "localhost";
    $user = "root";
    $password = "";
    $database = "ynsdb";

    // Connecting to the Database
    $conn = mysqli_connect($server, $user, $password, $database);

    // Check the connection
    if (!$conn) {
        echo "Database   error: " . mysqli_connect_error();
    }
?>  
<!DOCTYPE html>
<html>
    <head>
        <title>Retrieve employee's full name who has more than 2 positions.</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include 'includes/navbar.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                    <?php echo "<div class='alert alert-warning alert-dismissible fade show' role='alert'>
                       Retrieve employee's full name who has more than 2 positions.</div>"; 
                    ?>
                </div>
                <!-- Right side content -->
                <div class="col-lg-6 col-md-6 form-container">
                    <!-- Table -->
                    <div class="bs-example">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>first_name</th>
                                    <th>middle_name</th>
                                    <th>last_name</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $sql = "SELECT first_name, middle_name, last_name, 
                                            (SELECT COUNT(employee_id) FROM employee_positions 
                                            WHERE employee_positions.employee_id = employees.id) as count_p FROM employees
                                            WHERE (SELECT COUNT(employee_id) FROM employee_positions 
                                            WHERE employee_positions.employee_id = employees.id) > 1";
                                    $result = mysqli_query($conn, $sql);
                                    while ($row = mysqli_fetch_assoc($result)) {
                                        echo "<tr>";
                                            echo "<td>" . $row['first_name'] . "</td>";
                                            echo "<td>" . $row['middle_name'] . "</td>";
                                            echo "<td>" . $row['last_name'] . "</td>";
                                        echo "</tr>";
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>