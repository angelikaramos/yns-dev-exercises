<?php

    if (isset($_POST['Submit'])) {
        // Data from form
        $date = $_POST['date'];

        // Input date
        $input_date = date('Y-m-d', strtotime($date));

        // Result date
        $result = date('Y-m-d (l)', strtotime($date . '+ 3 days'));
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Input date. Then show 3 days from inputted date and its day of the week.</title>
       <!-- CSS only -->
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include 'includes/navbar.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                </div>
                <!-- Right side form -->
                <div class="col-lg-6 col-md-6 form-container">
                    <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
                        <!-- Logo -->
                        <div class="logo mb-3">
                            <img src="assets/img/yns.png" alt="yns_logo" width="150px">
                        </div>
                        <!-- Heading or Title of Exercise -->
                        <div class="heading mb-4">
                            <h4>3 Days from Inputted Date</h4>
                        </div>
                        <!-- Form -->
                        <form method="post" action="1-5.php">
                            <div class="form-input">
                                <label for="date">Date: </label><br>
                                <input type="date" name="date"><br><br>
                            </div>
                            <label for="input_date">Input Date:
                                <?php
                                    if (isset($input_date)) {
                                        echo $input_date;
                                    }
                                ?>
                            </label><br>
                            <label for="result">Result: 
                                <?php
                                    if (isset($result)) {
                                        echo $result;
                                    }
                                ?>
                            </label><br><br>
                            <button type="submit" name="Submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>