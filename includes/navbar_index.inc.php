<div class="jumbotron" style="margin-bottom:0px">
    <h1 class="text-danger">YNS Dev Exercises</h1>
    <p>Fun programming with us.</p>
</div>
<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top">
    <button class="navbar-toggler" data-toggle="collapse" data-target="#collapse_target">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="collapse_target">
        <span class="navbar-text"><a class="text-decoration-none" href="../5-3/5-3.php">YNS DEV Exercises</a></span>
        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" data-target="php_html" href="#">
                    PHP/HTML
                    <span class="caret"></span>
                </a>
                <div class="dropdown-menu" aria-labelledby="php_html">
                    <a class="dropdown-item" href="../1-1.php">Exercise 1-1</a>
                    <a class="dropdown-item" href="../1-2.php">Exercise 1-2</a>
                    <a class="dropdown-item" href="../1-3.php">Exercise 1-3</a>
                    <a class="dropdown-item" href="../1-4.php">Exercise 1-4</a>
                    <a class="dropdown-item" href="../1-5.php">Exercise 1-5</a>
                    <a class="dropdown-item" href="../csv_1-6/1-6.php">Exercise 1-6</a>
                    <a class="dropdown-item" href="../csv_1-7/1-7.php">Exercise 1-7</a>
                    <a class="dropdown-item" href="../csv_1-8/1-8.php">Exercise 1-8</a>
                    <a class="dropdown-item" href="../csv_1-9/1-9.php">Exercise 1-9</a>
                    <a class="dropdown-item" href="../csv_1-10/1-10.php">Exercise 1-10</a>
                    <a class="dropdown-item" href="../csv_1-11/1-11.php">Exercise 1-11</a>
                    <a class="dropdown-item" href="../csv_1-12/1-12.php">Exercise 1-12</a>
                    <a class="dropdown-item" href="../csv_1-13/1-13.php">Exercise 1-13</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" data-target="javascript" href="../#">
                    JavaScript
                    <span class="caret"></span>
                </a>
                <div class="dropdown-menu" aria-labelledby="javascript">
                    <a class="dropdown-item" href="../2-1.php">Exercise 2-1</a>
                    <a class="dropdown-item" href="../2-2.php">Exercise 2-2</a>
                    <a class="dropdown-item" href="../2-3.php">Exercise 2-3</a>
                    <a class="dropdown-item" href="../2-4.php">Exercise 2-4</a>
                    <a class="dropdown-item" href="../2-5.php">Exercise 2-5</a>
                    <a class="dropdown-item" href="../2-6.php">Exercise 2-6</a>
                    <a class="dropdown-item" href="../2-7.php">Exercise 2-7</a>
                    <a class="dropdown-item" href="../2-8.php">Exercise 2-8</a>
                    <a class="dropdown-item" href="../2-9.php">Exercise 2-9</a>
                    <a class="dropdown-item" href="../2-10.php">Exercise 2-10</a>
                    <a class="dropdown-item" href="../2-11.php">Exercise 2-11</a>
                    <a class="dropdown-item" href="../2-12.php">Exercise 2-12</a>
                    <a class="dropdown-item" href="../2-13.php">Exercise 2-13</a>
                    <a class="dropdown-item" href="../2-14.php">Exercise 2-14</a>
                    <a class="dropdown-item" href="../2-15.php">Exercise 2-15</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" data-target="database" href="../#">
                    Database
                    <span class="caret"></span>
                </a>
                <div class="dropdown-menu" aria-labelledby="database">
                    <a class="dropdown-item" href="../db_1-6/1-6.php">Exercise 1-6</a>
                    <a class="dropdown-item" href="../db_1-7/1-7.php">Exercise 1-7</a>
                    <a class="dropdown-item" href="../db_1-8/1-8.php">Exercise 1-8</a>
                    <a class="dropdown-item" href="../db_1-9/1-9.php">Exercise 1-9</a>
                    <a class="dropdown-item" href="../db_1-10/1-10.php">Exercise 1-10</a>
                    <a class="dropdown-item" href="../db_1-11/1-11.php">Exercise 1-11</a>
                    <a class="dropdown-item" href="../db_1-12/1-12.php">Exercise 1-12</a>
                    <a class="dropdown-item" href="../db_1-13/1-13.php">Exercise 1-13</a>
                    <a class="dropdown-item" href="../3-4_1.php">Exercise 3-4_1</a>
                    <a class="dropdown-item" href="../3-4_2.php">Exercise 3-4_2</a>
                    <a class="dropdown-item" href="../3-4_3.php">Exercise 3-4_3</a>
                    <a class="dropdown-item" href="../3-4_4.php">Exercise 3-4_4</a>
                    <a class="dropdown-item" href="../3-4_5.php">Exercise 3-4_5</a>
                    <a class="dropdown-item" href="../3-4_6.php">Exercise 3-4_6</a>
                    <a class="dropdown-item" href="../3-4_7.php">Exercise 3-4_7</a>
                    <a class="dropdown-item" href="../3-4_8.php">Exercise 3-4_8</a>
                    <a class="dropdown-item" href="../3-4_9.php">Exercise 3-4_9</a>
                    <a class="dropdown-item" href="../3-4_10.php">Exercise 3-4_10</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" data-target="advanceddb" href="../#">
                    Advanced SQL
                    <span class="caret"></span>
                </a>
                <div class="dropdown-menu" aria-labelledby="avanceddb">
                    <a class="dropdown-item" href="../7-1.php">Exercise 7-1</a>
                    <a class="dropdown-item" href="../7-2.php">Exercise 7-2</a>
                    <a class="dropdown-item" href="../7-3.php">Exercise 7-3</a>
                    <a class="dropdown-item" href="../7-4.php">Exercise 7-4</a>
                </div>
            </li>
        </ul>
    </div>
</nav>