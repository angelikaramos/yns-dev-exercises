<!DOCTYPE html>
<html>
    <head>
        <title>Change background color using animation.</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
        <script>
            window.addEventListener('DOMContentLoaded', (event) => {
                // Get body element
                var body = document.body;

                // Colors
                var color = ["rgb(0,22,43)","rgb(0,31,63)","rgb(0,41,82)","rgb(0,51,102)","rgb(0,61,122)",
                "rgb(0,71,141)","rgb(0,80,161)","rgb(0,90,180)","rgb(0,100,200)","rgb(0,110,220)",
                "rgb(0,120,239)","rgb(4,129,255)","rgb(24,139,255)"];

                // Initialize variable
                var i = 0;

                function changeBgColor() {
                    body.style.backgroundColor = color[i];
                    // Change the value of i
                    i++;
                }
                changeBgColor(); 

                setInterval(changeBgColor,200);
            });
        </script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include 'includes/navbar.inc.php'; ?> 
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                </div>
                <!-- Right side content -->
                <div class="col-lg-6 col-md-6 form-container">
                    <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
                        <!-- Logo -->
                        <div class="logo mb-3">
                            <img src="assets/img/yns.png" alt="yns_logo" width="150px">
                        </div>
                        <!-- Heading or Title of Exercise -->
                        <div class="heading mb-4">
                            <h4>Change Background Color using Animation</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </body>
</html>