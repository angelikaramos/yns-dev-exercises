-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2020 at 02:38 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yns_userdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_number` varchar(11) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `profile_picture` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `first_name`, `last_name`, `email`, `mobile_number`, `user_id`, `password`, `profile_picture`) VALUES
(1, 'Angel', 'Ramos', 'angelikaramos.yns@gmail.com', '09288283884', 'angel', '$2y$10$Jgdcjp1HvnXqzDZkCbv0Du.WuFmNIt8uxlSYcvmNkU3odDCOkcbF2', ''),
(2, 'Kathryn', 'Bernardo', 'kathrynbernardo@gmal.com', '09017262378', 'kathryn26', '$2y$10$iu8fryYq1FrxFvHyPYWwMud9EU7/z8dVSb79bLR3HrKqJ3uQaKoeS', ''),
(3, 'Liza', 'Soberano', 'lizahope@gmail.com', '09023723823', 'liza22', '$2y$10$RggeXfpazTtanllajlY8VOziY/abCiVe/.wZek3q3ELOUJIWas93S', ''),
(4, 'Nadine', 'Lustre', 'nadslustre@gmail.com', '09091623876', 'nadzz', '$2y$10$TSfxyVP47u26Axi0dhPHMO5mdbfO1jH4SC5uuYfqgORrUcMa3MJ9S', ''),
(5, 'Janella', 'Salvador', 'jsalvador@gmail.com', '09067267162', 'janella12', '$2y$10$gILpg1KJc1BYcCb4HIQbsuidca.kihdltxpxjiD0/O28hs0WD.lQi', ''),
(6, 'Mike', 'Lopez', 'mikelopez@gmail.com', '09017281621', 'mike12', '$2y$10$gtvSdnQv2k1JriLg.8Ihh.P/J1ehKjsfcQM/YUt2XupOvP9XjcP.i', 'mario.jpg'),
(7, 'Kim', 'Possible', 'kimpossible@gmail.com', '09017271627', 'kim23', '$2y$10$6K/WZPQw2Unq/WvqVODttOgoPznxMYyPfcHhEl8UJFM/K1MH.u096', 'profile11.png'),
(8, 'Ron', 'Stoppable', 'ronstoppable@gmail.com', '09012716217', 'ron34', '$2y$10$FjIeRNxrOF1DhZ0t7tXZC.4fHlcMdEytYpRnpV3lYwjFT1ZyMU5Zy', 'profile13.jpg'),
(9, 'Shego', 'Villain', 'shegovilain@gmail.com', '09016872637', 'shego45', '$2y$10$IeXMcYOc8jUnZhQdB2RY8etD5GeTx8hY2j9IbZIzArzcgXtm/eUrG', 'profile3.jpg'),
(10, 'Maria', 'Arevalo', 'maria.arevalo@gmail.com', '09067263276', 'maria11', '$2y$10$jfl7v7S73899zkmnz4wIL.hIh2hkBsEaIPS4lyVFsh21qqg.ytF4a', 'profile1.jpg'),
(11, 'Anne', 'Garcia', 'annegarcia@gmail.com', '09082783172', 'anne344', '$2y$10$lT73Ng1h2Eqkg5OrwSd4c.fisohvru1BckClqxz3IPl64XB4kRiiK', 'profile8.jpg'),
(12, 'Lovely', 'Perez', 'lovelyperez@gmail.com', '09072837237', 'loverly887', '$2y$10$8htZJooHGfJWlQc1RqB1BeY5m/bzdZf.5N7inx/h4OzWXNCXLmddi', 'profile7.jpg'),
(13, 'Christine', 'Cruz', 'christinecruz@gmail.com', '09019716281', 'christine112', '$2y$10$li76ZdP4HaSRT6Z5weZr5umPUKQgKunRm/As.ktRzBohHvxEgTkya', 'profile14.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
