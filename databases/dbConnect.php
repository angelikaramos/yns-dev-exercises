<?php

    $server = "localhost";
    $user = "root";
    $password = "";
    $database = "yns_userdb";

    // Connection to the Database
    $conn = mysqli_connect($server, $user, $password, $database);
    
    // Check the connection
    if (!$conn) {
        echo "Database error: " . mysqli_connect_error();
    }
?>