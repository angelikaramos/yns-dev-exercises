<!DOCTYPE html>
<html>
    <head>
        <title>Show another image when you mouse over an image. Then show the original image when you mouse out.</title>
         <!-- CSS only -->
         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
        <script>
            window.addEventListener('DOMContentLoaded', (event) => {
                // Get html element by id
                var img = document.getElementById('img');

                // Change image when the mouse moves in
                img.addEventListener('mouseover', function() {
                    img.src = "assets/img/kim.png";
                });
                
                // Back to original image when the mouse moves out
                img.addEventListener('mouseout', function() {
                    img.src = "assets/img/shego.jpg";
                });
            });
        </script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include 'includes/navbar.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                </div>
                <!-- Right side form -->
                <div class="col-lg-6 col-md-6 form-container">
                    <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
                        <!-- Logo -->
                        <div class="logo mb-3">
                            <img src="assets/img/yns.png" alt="yns_logo" width="150px">
                        </div>
                        <!-- Heading or Title of Exercise -->
                        <div class="heading mb-4">
                            <h4>Show another Image when MouseOver and MouseOut</h4>
                        </div>
                        <!-- Image -->
                        <div class="form-input">
                            <img src="assets/img/shego.jpg" id="img" style="height:100%;width:50%">
                        </div>        
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>