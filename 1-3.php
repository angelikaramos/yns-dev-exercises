<?php

    if (isset($_POST['Submit'])) {
        // Data from form
        $first_number = $_POST['first_number'];
        $second_number = $_POST['second_number'];

        //Initialize variable
        $gcds = array();
    
        // Getting GCD
        if ($first_number == $second_number) { // If both number are the same get the second number
            $result = $second_number; 
        } elseif ($first_number < $second_number) { // If first number is less than second number
            if (($second_number % $first_number) == 0) { 
                $result = $first_number;
            } elseif (($first_number % $second_number) != 0) { 
                for($gcd = 1; $gcd != 0;) { // For loop: If GCD is equal to 0 end of loop
                    $gcds[] = $gcd; // Store in array all the candidates in GCD
                    
                    $quotient = intdiv($second_number,$first_number); 
                    $gcd = $second_number-($quotient*$first_number); /* Get candidate GCD by subtracting the second number to 
                                                                        the product of quotient and first number */
                    $second_number = $first_number; 
                    $first_number = $gcd; 
                }
                $result = end($gcds); // Last element will be the GCD
            }
        } elseif ($first_number > $second_number) { // If first number is greater than second number
            if (($first_number % $second_number) == 0) { 
                $result = $second_number;
            } elseif (($first_number % $second_number) != 0) { 
                for($gcd = 1; $gcd != 0;) { // For loop: If GCD is equal to 0 end of loop
                    $gcds[] = $gcd; // Store in array all the candidates in GCD
                    
                    $quotient = intdiv($first_number,$second_number); 
                    $gcd = $first_number-($quotient*$second_number); /* Get candidate GCD by subtracting the first number to 
                                                                        the product of quotient and second number */
                    $first_number = $second_number; 
                    $second_number = $gcd; 
                }
                $result = end($gcds); // Last element will be the GCD
            }
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Show the Greatest Common Divisor</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include 'includes/navbar.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                </div>
                <!-- Right side form -->
                <div class="col-lg-6 col-md-6 form-container">
                    <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
                        <!-- Logo -->
                        <div class="logo mb-3">
                            <img src="assets/img/yns.png" alt="yns_logo" width="150px">
                        </div>
                        <!-- Heading or Title of Exercise -->
                        <div class="heading mb-4">
                            <h4>Greatest Common Divisor</h4>
                        </div>
                        <!-- Form -->
                        <form method="post" action="1-3.php">
                            <div class="form-input">
                                <label for="first_number">First Number:</label><br>
                                <input type="text" name="first_number" required><br><br>
                            </div>
                            <div class="form-input">
                                <label for="second_number">Second Number:</label><br>
                                <input type="text" name="second_number" required><br><br>
                            </div>
                            <label for="result">Result :
                            <?php 
                                if (isset($result)) {
                                    echo $result;
                                }
                            ?>
                            </label><br><br>
                            <button type="submit" name="Submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>