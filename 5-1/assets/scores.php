<?php

    session_start();

    include_once '../includes/dbConnect.inc.php';
    include '../queries/scoreQueries.php';

    // Session Variables
    $user_id = $_SESSION['id'];
    $username = $_SESSION['username']; 

    // Can't go to this page unless they login
    if (empty($_SESSION['id']) && empty($_SESSION['username'])) {
        header("Location: ../index.php");
    }

    // Set Database
    $quizTimeDB = new QuizTimeDB();
    $connection = $quizTimeDB->GetMySQLIConnection();

    // Classes 
    $scoreQueries = new ScoreQueries($connection);

    // Get scores
    $scores = $scoreQueries->getScores($user_id);
?>
<!DOCTYPE html>
<html lang="en">
    <!-- Header -->
    <?php include '../includes/header.inc.php'; ?>
    <body class="sb-nav-fixed">
        <!-- Navigation Bar -->
        <?php include '../includes/navbar.inc.php'; ?>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-light" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Core</div>
                            <a class="nav-link" href="user_index.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-home text-dark"></i></div>
                                <strong>HOME</strong> 
                            </a>
                            <a class="nav-link" href="quiz.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-question text-dark"></i></div>
                                <strong>QUIZ</strong> 
                            </a>
                            <a class="nav-link" href="scores.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-star text-dark"></i></div>
                                <strong>SCORES</strong> 
                            </a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        <?php echo ucfirst($username); ?>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">SCORES</h1>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <tr>
                                    <th>ID</th>
                                    <th>Score</th>
                                    <th>Date</th>
                                </tr>
                                <?php
                                 if ($scores == null) {
                                    echo "<tr>";
                                        echo "<td colspan='3' style='text-align:center'> NO DATA</td>";
                                    echo "</tr>";
                                }
                                if ($scores != null) {
                                    foreach($scores as $score) {
                                        echo "<tr>";
                                            echo "<td>" . $score->id . "</td>";
                                            echo "<td>" . $score->score . " / 10 </td>";
                                            echo "<td>" . $score->date . "</td>";
                                        echo "</tr>";
                                    }
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; QUIZTIME! 2020</div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </body>
</html>
