<?php
    session_start();

    include_once '../includes/dbConnect.inc.php';
    include '../queries/quizQueries.php';
    include '../queries/usedQuestionQueries.php';
    include '../queries/scoreQueries.php';

    // Can't go to this page unless they login
    if (empty($_SESSION['id']) && empty($_SESSION['username'])) {
        header("Location: ../index.php");
    }

    if (isset($_GET['id'])) {
        $question_id = $_GET['id'];
    }

    // Set Database
    $quizTimeDB = new QuizTimeDB();
    $connection = $quizTimeDB->GetMySQLIConnection();

    // Session Variables
    $user_id = $_SESSION['id'];
    $username = $_SESSION['username']; 
    
    // Classes
    $quizQueries = new QuizQueries($connection);
    $usedQuestionQueries = new UsedQuestionQueries($connection);
    $scoreQueries = new ScoreQueries($connection);

    // Check if used_question already inserted
    $check = $usedQuestionQueries->insertUsedQuestion($user_id, $question_id);

    // Count correct answers
    $score = $quizQueries->countCorrectAnswer($user_id);

    // Record final score
    $recordScore = $scoreQueries->insertScore($user_id, $score);
?>
<!DOCTYPE html>
<html lang="en">
    <!-- Header -->
    <?php include '../includes/header.inc.php'; ?>
    <body class="sb-nav-fixed">
        <!-- Navigation Bar -->
        <?php include '../includes/navbar.inc.php'; ?>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-light" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Core</div>
                            <a class="nav-link" href="user_index.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-home text-dark"></i></div>
                                <strong>HOME</strong> 
                            </a>
                            <a class="nav-link active" href="quiz.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-question text-dark"></i></div>
                                <strong>QUIZ</strong> 
                            </a>
                            <a class="nav-link" href="scores.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-star text-dark"></i></div>
                                <strong>SCORES</strong> 
                            </a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        <?php echo ucfirst($username); ?>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <div class="row">
                            <img src="img/score.jpg" class="img-responsive" style="margin:0 auto;width:40%" 
                            alt="score_quiz">
                        </div>
                        <div class="text-center score-container mb-5">
                            <strong><p><?php echo strtoupper($username) ."'S SCORE: " . $score . " / 10"; ?>
                            </p></strong>    
                        </div>
                        <div class="row d-flex justify-content-center">
                            <a href="../includes/play_again.inc.php"><button type="submit" 
                            class="btn btn-success text-uppercase rounded-pill btn-lg">Play Again</button></a>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; QUIZTIME! 2020</div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </body>
</html>
