<?php

    session_start();

    include_once '../includes/dbConnect.inc.php';
    include '../queries/questionQueries.php';
    include '../queries/optionQueries.php';
    include '../queries/usedQuestionQueries.php';

    // Can't go to this page unless they login
    if (empty($_SESSION['id']) && empty($_SESSION['username'])) {
        header("Location: ../index.php");
    }

    // Set Database
    $quizTimeDB = new QuizTimeDB();
    $connection = $quizTimeDB->GetMySQLIConnection();

    // Session Variables
    $user_id = $_SESSION['id'];
    $username = $_SESSION['username'];

    // Set Question Number
    $usedQuestionQueries = new UsedQuestionQueries($connection);
    $question_number = $usedQuestionQueries->countUsedQuestions($user_id)+1;

    if (isset($_SESSION['random_id'])) {
        $randomQuestionId = $_SESSION['random_id'][$question_number-1];
    }

    // Retrieve from tbl_questions
    $questionQueries = new QuestionQueries($connection);
    $questions = $questionQueries->getQuestionById($randomQuestionId);
    
    // Retrieve from tbl_operations
    $optionQueries = new OptionQueries($connection);
    $options = $optionQueries->getAllOptionsByQuestionId($randomQuestionId);
?>
<!DOCTYPE html>
<html lang="en">
    <!-- Header -->
    <?php include '../includes/header.inc.php'; ?>
    <body class="sb-nav-fixed">
        <!-- Navigation Bar -->
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-light">
            <!-- Logo -->
            <a class="navbar-brand text-center" href="user_index.php"><img src="../assets/img/logo_home.png" 
            alt="QUIZTIME! Logo"></a>
            <!-- Side Bar Toggle Button -->
            <button class="btn btn-link btn-sm order-1 order-lg-0 bg-d text-dark"  id="sidebarToggle" href="#">
            <i class="fas fa-bars"></i></button>
            <!-- Navbar -->
            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <div class="input-group">
                    <!-- <div class="input-group-append">
                        <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
                    </div> -->
                    <a class="dropdown-item logout" id="logout"><strong>LOGOUT</strong></a>
                </div>
            </form>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-light" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Core</div>
                            <a class="nav-link" id="home">
                                <div class="sb-nav-link-icon"><i class="fas fa-home text-dark"></i></div>
                                <strong>HOME</strong> 
                            </a>
                            <a class="nav-link active" id="quiz">
                                <div class="sb-nav-link-icon"><i class="fas fa-question text-dark"></i></div>
                                <strong>QUIZ</strong> 
                            </a>
                            <a class="nav-link" id="scores">
                                <div class="sb-nav-link-icon"><i class="fas fa-star text-dark"></i></div>
                                <strong>SCORES</strong> 
                            </a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        <?php echo ucfirst($username); ?>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <!-- Alert Message -->
                        <?php 
                            if (isset($_SESSION['success_message'])) {
                                echo $_SESSION['success_message'];
                                unset($_SESSION['success_message']);
                            }
                            if (isset($_SESSION['error_message'])) {
                                echo $_SESSION['error_message'];
                                unset($_SESSION['error_message']);
                            }
                        ?>
                        <!-- Next Question and See your Score button -->
                        <?php
                            if ($question_number < 10) {
                                echo "<a href='../includes/next_question.inc.php?id=$randomQuestionId'>
                                <button type='submit' class='btn btn-primary rounded-pill btn-lg float-right'>
                                    NEXT QUESTION</button></a>
                                <h1 class='mt-4'>QUESTION #". $question_number." / 10 </h1>";
                            }
                            if ($question_number == 10) {
                                echo "<a href='quiz_score.php?id=$randomQuestionId'>
                                <button type='submit' class='btn btn-info rounded-pill btn-lg float-right'>
                                    SEE YOUR SCORE</button></a>
                                <h1 class='mt-4'>QUESTION #". $question_number." / 10 </h1>";
                            }
                        ?>
                        <div class="row">
                            <div class="col-md-6 question-container border-right border-dark">
                                <p>
                                    <?php 
                                        // Displaying the random question
                                        foreach($questions as $question) {
                                            echo $question->questionText;
                                        }  
                                    ?>
                                </p><br>
                            </div>
                            <div class="col-md-6 button-group">
                                <?php
                                    foreach($options as $option) {
                                        // For inserting selected answer in tbl_quiz
                                        echo "<form action='../includes/quiz_answer.inc.php' method='post'>
                                        <input type='hidden' name='question_id' value='".$randomQuestionId."'>
                                        <input type='hidden' name='option_id' value='".$option->id."'>
                                        <button type='submit' name='answer' class='btn btn-success btn-block 
                                        rounded-pill btn-lg mb-5'>".$option->optionText."</button></form>";
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; QUIZTIME! 2020</div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </body>
</html>
