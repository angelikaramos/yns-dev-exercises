<?php

    session_start();

    // Session Variables
    $user_id = $_SESSION['id'];
    $username = $_SESSION['username']; 

    // Can't go to this page unless they login
    if (empty($_SESSION['id']) && empty($_SESSION['username'])) {
        header("Location: ../index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
    <!-- Header -->
    <?php include '../includes/header.inc.php'; ?>
    <body class="sb-nav-fixed">
        <!-- Navigation Bar -->
        <?php include '../includes/navbar.inc.php'; ?>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-light" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Core</div>
                            <a class="nav-link active" href="user_index.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-home text-dark"></i></div>
                                <strong>HOME</strong> 
                            </a>
                            <a class="nav-link" href="quiz.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-question text-dark"></i></div>
                                <strong>QUIZ</strong> 
                            </a>
                            <a class="nav-link" href="scores.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-star text-dark"></i></div>
                                <strong>SCORES</strong> 
                            </a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        <?php echo ucfirst($username); ?>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">QUIZTIME!</h1>
                        <div class="row">
                            <div class="col-md-6">
                                <p>is a web application that lets you test your brain in 
                                    answering Computer-related questions. This is perfect for users
                                    who want to recall their general knowledge in Computer Science.</p><br><br>
                                <h2>INSTRUCTIONS:</h2>
                                <ul>
                                    <li>Each question of this quiz is multiple-choice question.</li>
                                    <li>Read each question carefully.</li>
                                    <li>Click the button that corresponds to your answer.</li>
                                    <li>After answering, click on the "Next Question" button at the 
                                        upper right of the page.</li>
                                    <li>You must answer the ten questions or else your quiz will
                                        not be scored.</li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <img src="img/home.png" class="float-right" alt="online_education" 
                                style="width:60%">
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; QUIZTIME! 2020</div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </body>
</html>
