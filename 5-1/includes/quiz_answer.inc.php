<?php
    
    session_start();

    include_once 'dbConnect.inc.php';
    include '../queries/questionQueries.php';
    include '../queries/optionQueries.php';
    include '../queries/answerQueries.php';
    include '../queries/quizQueries.php';

    // Session Variable
    $user_id = $_SESSION['id'];

    // Set Database
    $quizTimeDB = new QuizTimeDB();
    $connection = $quizTimeDB->GetMySQLIConnection();

    // Classes
    $answerQueries = new AnswerQueries($connection);
    $quizQueries = new QuizQueries($connection);
    $optionQueries = new OptionQueries($connection);

    // Get hidden data from form
    $question_id = $_POST['question_id'];
    $option_id = $_POST['option_id'];

    // Get answer_id to tbl_answers
    $answer_id = $answerQueries->getAnswerIdByQuestionId($question_id);

    // Get answer_text to tbl_answers
    $answer_text = $answerQueries->getAnswerTextByQuestionId($question_id);

    // Get option_text to tbl_options
    $option = $optionQueries->getOptionTextByOptionId($option_id);

    // To be inserted in tbl_quiz
    $quiz = new Quiz();
    $quiz->user_id = $user_id;
    $quiz->question_id = $question_id;
    $quiz->answer_id = $answer_id->id;
    $quiz->option_id = $option_id;

    // Button submit
    $answer = isset($_POST['answer']);

    // Validate
    if (!$answer) {
        return;
    }

    // Check if user has already an answer
    $count = $quizQueries->countQuestion($user_id, $question_id);

    // Update if user has already an answer
    if ($count > 0){
        // Update option_id to tbl_quiz
        $updateAnswer = $quizQueries->updateAnswer($quiz);

        // Check if data are updated
        if ($updateAnswer == 1) {
            // Check if answer is correct
            if ($option->optionText == $answer_text->answerText) {
                // Record correct answer
                $correct = $quizQueries->recordCorrectAnswer($question_id);
                if ($correct == 1) {
                    $_SESSION['success_message'] = "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                    <strong>Correct!</strong> Your answer is <strong>". $option->optionText . "</strong>.
                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>
                    </div>";
                    header("Location: ../assets/quiz_questions.php?id=".$question_id."");
                    exit();
                }
                if ($correct == 0) {
                    $_SESSION['error_message'] = "<strong>Sorry!</strong> Error in recording correct answer";
                    header("Location: ../assets/quiz_questions.php?id=".$question_id."");
                    exit();
                }
            
            }
            if ($option->optionText != $answer_text->answerText) {
                // Record wrong answer
                $wrong = $quizQueries->recordWrongAnswer($question_id);
                $_SESSION['error_message'] = "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
                <strong>Sorry!</strong> Your answer is wrong.
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
                </button>
                </div>";
                header("Location: ../assets/quiz_questions.php?id=".$question_id."");
                exit();
            }
        }
        if ($updateAnswer == 0) {
            $_SESSION['error_message'] = "<strong>Sorry!</strong> Error in submitting answer";
            header("Location: ../assets/quiz_questions.php?id=".$question_id."");
            exit();
        }
    }
    // Insert answer if user has no answer yet
    if ($count == 0) {
        // Insert user, answer, question and option data to tbl_quiz
        $insertAnswer = $quizQueries->insertAnswer($quiz);

        // Check if all data are inserted
        if ($insertAnswer == 1) {
            // Check if answer is correct
            if ($option->optionText == $answer_text->answerText) {
                // Record correct answer
                $correct = $quizQueries->recordCorrectAnswer($question_id);
                if ($correct == 1) {
                    $_SESSION['success_message'] = "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                    <strong>Correct!</strong> Your answer is <strong>". $option->optionText . "</strong>.
                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>
                    </div>";
                    header("Location: ../assets/quiz_questions.php?id=".$question_id."");
                    exit();
                }
                if ($correct == 0) {
                    $_SESSION['error_message'] = "<strong>Sorry!</strong> Error in recording correct answer";
                    header("Location: ../assets/quiz_questions.php?id=".$question_id."");
                    exit();
                }
            
            }
            if ($option->optionText != $answer_text->answerText) {
                $_SESSION['error_message'] = "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
                <strong>Sorry!</strong> Your answer is wrong.
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
                </button>
                </div>";
                header("Location: ../assets/quiz_questions.php?id=".$question_id."");
                exit();
            }
        }
        if ($insertAnswer == 0) {
            $_SESSION['error_message'] = "<strong>Sorry!</strong> Error in submitting answer";
            header("Location: ../assets/quiz_questions.php?id=".$question_id."");
            exit();
        }
    }     
    
   
   