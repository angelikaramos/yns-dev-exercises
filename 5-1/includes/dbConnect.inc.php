<?php

    interface IDB {
        function GetMySQLIConnection(): mysqli;
    }

    class QuizTimeDB implements IDB {
        protected $server = "localhost";
        protected $username = "root";
        protected $password = "";
        protected $database = "quiztimedb";
    
        public function GetMySQLIConnection(): mysqli {
        return new mysqli($this->server, $this->username, $this->password, $this->database);
        }
    }