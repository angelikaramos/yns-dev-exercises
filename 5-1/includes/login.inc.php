<?php

    session_start();

    include 'dbConnect.inc.php';
    include '../queries/userQueries.php';

    // Set Database
    $quiztimeDB = new QuizTimeDB();
    $connection = $quiztimeDB->GetMySQLIConnection();

    // Classes
    $userQueries = new UserQueries($connection);

    // Get data from form
    $username = mysqli_real_escape_string($connection, $_POST['username']);
    $password = mysqli_real_escape_string($connection, $_POST['password']);

    $user = new User();
    $user->username = $username;
    $user->password = $password;

    if (isset($_POST['login'])) {

        // Validations
        if (empty($username) && !empty($password)) {
            $_SESSION['error_message'] = "Enter your username";
            header("Location: ../index.php");
            exit();
        }
        if (empty($password) && !empty($username)) {
            $_SESSION['error_message'] = "Enter your password";
            header("Location: ../index.php");
            exit();
        }
        if (empty($username) && empty($password)) {
            $_SESSION['error_message'] = "Enter your username and password";
            header("Location: ../index.php");
            exit();
        }

        // Check if username exists in database
        $count = $userQueries->countUsersByUserName($username);
        
        // Username is not valid
        if ($count == 0) {
            $_SESSION['error_message']= "Username not found";
            header("Location: ../index.php");
            exit();
        }

        // Username is valid
        if ($count == 1) {
            // Check if password is valid
            $check = $userQueries->checkPassword($user);
            if ($check == 1) {
                // Get data id and username
                $users = $userQueries->getUserByUserName($username);
                foreach($users as $user) {
                    // Set SESSION variables
                    $_SESSION['id'] = $user->id;
                    $_SESSION['username'] = $user->username;
                    header("Location: ../assets/user_index.php");
                    exit();
                }
            }
            if ($check == 0) {
                $_SESSION['error_message'] = "Password is invalid";
                header("Location: ../index.php");
                exit();
            }
        }
    }