<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>QUIZTIME!</title>
    <link rel="stylesheet" href="../assets/css/template.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" ></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" defer></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" defer></script>
    <script src="../assets/js/scripts.js" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" defer></script>
    <script>
        window.addEventListener('DOMContentLoaded', (event) => {
            var home = document.getElementById('home');
            var quiz = document.getElementById('quiz');
            var logout = document.getElementById('logout');
            var scores = document.getElementById('scores');

            // When home side nav bar clicked
            home.addEventListener('click', function() {
                var response;
                var message = "You must complete to answer the ten questions or else your quiz will not be scored. Do you want to continue?";
                response = confirm(message);
                if (response == true) {
                    location.replace('../includes/truncatedata.inc.php?route=home');
                }
            });

            // When quiz side nav bar clicked
            quiz.addEventListener('click', function() {
                var response;
                var message = "You must complete to answer the ten questions or else your quiz will not be scored. Do you want to continue?";
                response = confirm(message);
                if (response == true) {
                    location.replace('../includes/truncatedata.inc.php?route=quiz');
                }
            });

            // When scores at nav bar clicked
            scores.addEventListener('click', function() {
                var response;
                var message = "You must complete to answer the ten questions or else your quiz will not be scored. Do you want to continue?";
                response = confirm(message);
                if (response == true) {
                    location.replace('../includes/truncatedata.inc.php?route=scores');
                }
            });

            // When logout at nav bar clicked
            logout.addEventListener('click', function() {
                var response;
                var message = "You must complete to answer the ten questions or else your quiz will not be scored. Do you want to continue?";
                response = confirm(message);
                if (response == true) {
                    location.replace('../includes/truncatedata.inc.php?route=logout');
                }
            });
        });

    </script>
</head>