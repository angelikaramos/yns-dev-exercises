<?php

    session_start();

    include_once 'dbConnect.inc.php';
    include '../queries/questionQueries.php';
    include '../queries/usedQuestionQueries.php';

    // Session Variable
    $user_id = $_SESSION['id'];

    // Set Database
    $quiztimeDB = new QuizTimeDB();
    $connection = $quiztimeDB->GetMySQLIConnection();

    // Classes
    $usedQuestionQueries = new UsedQuestionQueries($connection);
    $questionQueries = new QuestionQueries($connection);
   
    $question_id = isset($_GET['id']);
    
    // Validate
    if (!$question_id) {
        return;
    }

    // Check if used_question already inserted
    $check = $usedQuestionQueries->insertUsedQuestion($user_id, $question_id);

    // Count total used questions
    $usedQuestionQueries = new UsedQuestionQueries($connection);
    $question_number = $usedQuestionQueries->countUsedQuestions($user_id);

    // Get next value from session array
    if (isset($_SESSION['random_id'])) {
        $randomQuestionId = $_SESSION['random_id'][$question_number];
    }

    // Retrieve question from tbl_questions using next value from session array
    $questionQueries = new QuestionQueries($connection);
    $questions = $questionQueries->getQuestionById($randomQuestionId);

    // Back to quiz questions page
    foreach($questions as $question) {
        header("Location: ../assets/quiz_questions.php?id=".$question->id."");
    }
   

