<?php

    session_start();

    include 'dbConnect.inc.php';
    include '../queries/userQueries.php';

    // Set Database
    $quiztimeDB = new QuizTimeDB();
    $connection = $quiztimeDB->GetMySQLIConnection();

    // Classes
    $userQueries = new UserQueries($connection);

    // Get data from form
    $email = $_POST['email'];
    $username = $_POST['username'];
    $password = $_POST['password'];

    // Password Hashing
    $password = password_hash($password, PASSWORD_DEFAULT);

    $user = new User();
    $user->email = $email;
    $user->username = $username;
    $user->password = $password;

    $register = isset($_POST['register']);
    
    // Validate
    if (!$register) {
        return;
    }
    
    // Validations
    if (empty($email) && !empty($username) && !empty($password)) {
        $_SESSION['error_message'] = "Enter your Email";
        header("Location: ../assets/register.php?error=emptyfield");
        exit();
    }
    if (empty($username) && !empty($email) && !empty($password)) {
        $_SESSION['error_message'] = "Enter your Username";
        header("Location: ../assets/register.php?error=emptyfield");
        exit();
    }
    if (empty($password) && !empty($email) && !empty($username)) {
        $_SESSION['error_message'] = "Enter your Password";
        header("Location: ../assets/register.php?error=emptyfield");
        exit();
    }
    if (empty($email) && empty($username) && empty($password)) {
        $_SESSION['error_message'] = "Enter Email, Username and Password";
        header("Location: ../assets/register.php?error=emptyfields");
        exit();
    }

    // Check if username exists in Database
    $count = $userQueries->countUsersByUsername($username);

    if ($count > 0) {
        $_SESSION['error_message'] = "Username already exists";
        header("Location: ../assets/register.php?error=username already exists");
        exit();
    }

    // Check if all data are inserted
    $check = $userQueries->InsertUser($user);
    if ($check == 1) {
        $_SESSION['success_message'] = "Your registration has been successfully completed";
        header("Location: ../index.php?success=registration");
        exit();
    } 
    if ($check == 0) {
        $_SESSION['error_message'] = "Error in registration";
        header("Location: ../assets/register.php?failed=registration");
        exit();
    }