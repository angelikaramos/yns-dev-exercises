<?php

    session_start();

    include_once 'dbConnect.inc.php';
    include '../queries/usedQuestionQueries.php';
    include '../queries/quizQueries.php';

    // Session Variable
    $user_id = $_SESSION['id'];

    // $_GET Variable
    $route = $_GET['route'];

    // Set Database
    $quiztimeDB = new QuizTimeDB();
    $connection = $quiztimeDB->GetMySQLIConnection();

    // Classes
    $usedQuestionQueries = new UsedQuestionQueries($connection);
    $quizQueries = new QuizQueries($connection);

    // Check if data already truncated in tbl_used_questions, 1 = true, 0 = false
    $check_used_questions = $usedQuestionQueries->truncateUsedQuestions($user_id);
    if ($check_used_questions == 1) {
        // Check if data already truncated in tbl_quiz, 1 = true, 0 = false
        $check_quiz = $quizQueries->truncateAnswers($user_id);
        if ($check_quiz == 1) {
            // If all data are truncated, go to target route
            if ($route == "home") {
                header("Location: ../assets/user_index.php");
                exit();
            }
            if ($route == "quiz") {
                header("Location: ../assets/quiz.php");
                exit();
            }
            if ($route == "scores") {
                header("Location: ../assets/scores.php");
                exit();
            }
            if ($route == "logout") {
                header("Location: logout.inc.php");
                exit();
            }
        }
        if ($check_quiz == 0) {
            echo ">Sorry! Error resetting tbl_quiz";
        }
    }
    if ($check_used_questions == 0) {
        echo "Sorry!</strong> Error resetting tbl_used_questions";
        header("Location: ../assets/quiz_questions.php?id=");
        exit();
    }

