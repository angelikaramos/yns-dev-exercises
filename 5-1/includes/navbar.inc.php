<nav class="sb-topnav navbar navbar-expand navbar-dark bg-light">
    <!-- Logo -->
    <a class="navbar-brand text-center" href="user_index.php"><img src="../assets/img/logo_home.png" alt="QUIZTIME! Logo"></a>
    <!-- Side Bar Toggle Button -->
    <button class="btn btn-link btn-sm order-1 order-lg-0 bg-d text-dark"  id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
    <!-- Navbar -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
        <div class="input-group">
            <!-- <div class="input-group-append">
                <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
            </div> -->
            <a class="dropdown-item logout" id="logout" href="../includes/logout.inc.php"><strong>LOGOUT</strong></a>
        </div>
    </form>
</nav>