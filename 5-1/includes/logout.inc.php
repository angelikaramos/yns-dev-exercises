<?php 

    session_start();

    // Destroy the session
    session_destroy();

    // Back to login page
    header("Location: ../index.php");