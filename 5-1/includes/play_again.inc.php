<?php

    session_start();

    include_once '../includes/dbConnect.inc.php';
    include '../queries/usedQuestionQueries.php';
    include '../queries/quizQueries.php';

    // Set Database
    $quizTimeDB = new QuizTimeDB();
    $connection = $quizTimeDB->GetMySQLIConnection();

    // Session Variables
    $user_id = $_SESSION['id'];
    $username = $_SESSION['username'];  

    // Session array for getting random questions
    function randomGen($min, $max, $quantity) {
        $numbers = range($min, $max);
        shuffle($numbers);
        return array_slice($numbers, 0, $quantity);
    }

    // Array, randomized numbers from 1-10
    $random_id = randomGen(1,10,10);

    // Set new array session
    $_SESSION['random_id'] = $random_id;

    // Classes
    $usedQuestionQueries = new UsedQuestionQueries($connection);
    $quizQueries = new QuizQueries($connection);

    // Check if data already truncated in tbl_used_questions, 1 = true, 0 = false
    $check_used_questions = $usedQuestionQueries->truncateUsedQuestions($user_id);
    
    if ($check_used_questions == 1) {
        // Check if data already truncated in tbl_quiz, 1 = true, 0 = false
        $check_quiz = $quizQueries->truncateAnswers($user_id);
        if ($check_quiz == 1) {
            header("Location: ../assets/quiz_questions.php");
        }
        if ($check_quiz == 0) {
            echo ">Sorry! Error resetting tbl_quiz";
        }
    }
    if ($check_used_questions == 0) {
        echo "Sorry!</strong> Error resetting tbl_used_questions";
        header("Location: ../assets/quiz_questions.php?id=");
        exit();
    }