<?php

    include_once '../classes/option.php';

    interface IOptionQueries {
        public function getAllOptionsByQuestionId(int $question_id);
        public function getOptionTextByOptionId(int $option_id);
    }

    class OptionQueries implements IOptionQueries {
        private $conn;

        public function __construct($connection) {
            $this->conn = $connection;
        }

        public function getAllOptionsByQuestionId(int $question_id) {
            $sql = "SELECT * FROM tbl_options WHERE question_id = '$question_id'";
            $result = $this->conn->query($sql);

            $options = array();
            while ($row = $result->fetch_assoc()) {
                $option = new Option();

                $option->id = $row['id'];
                $option->optionText = $row['option_text'];

                array_push($options, $option);
            }

            return $options;
        }

        public function getOptionTextByOptionId(int $option_id) {
            $sql = "SELECT * FROM tbl_options WHERE id = '$option_id'";
            $result = $this->conn->query($sql);
            $row = $result->fetch_assoc();
            
            $option = new Option();
            $option->optionText = $row['option_text'];

            return $option;
        }
    }