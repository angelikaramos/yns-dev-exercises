<?php

    include_once '../classes/quiz.php';

    interface IQuizQueries {
        public function insertAnswer(Quiz $quiz);
        public function truncateAnswers(int $user_id);
        public function recordCorrectAnswer(int $question_id);
        public function recordWrongAnswer(int $question_id);
        public function countCorrectAnswer(int $user_id);
        public function countQuestion(int $user_id, int $question_id);
        public function updateAnswer(Quiz $quiz);
    }

    class QuizQueries implements IQuizQueries {
        private $conn;

        public function __construct($connection) {
            $this->conn = $connection;
        }

        public function insertAnswer(Quiz $quiz) {
            $sql = "INSERT INTO tbl_quiz (user_id, question_id, answer_id, option_id) VALUES ('$quiz->user_id', 
            '$quiz->question_id', '$quiz->answer_id', '$quiz->option_id')";
            $result = $this->conn->query($sql);

            if (!$result) {
                trigger_error('Invalid query: ' . $this->conn->error);
            }

            return $result == true ? 1 : 0 ;
        }

        public function truncateAnswers($user_id) {
            $sql = "DELETE FROM tbl_quiz WHERE user_id = '$user_id'";
            $result = $this->conn->query($sql);

            if (!$result) {
                trigger_error('Invalid query: ' . $this->conn->error);
            }

            return $result == true ? 1 : 0;
        }

        public function recordCorrectAnswer(int $question_id) {
            $sql = "UPDATE tbl_quiz SET is_correct = 1 WHERE question_id = '$question_id'";
            $result = $this->conn->query($sql);

            if (!$result) {
                trigger_error('Invalid query: ' . $this->conn->error);
            }

            return $result == true ? 1 : 0;
        }

        public function recordWrongAnswer(int $question_id) {
            $sql = "UPDATE tbl_quiz SET is_correct = 0 WHERE question_id = '$question_id'";
            $result = $this->conn->query($sql);

            if (!$result) {
                trigger_error('Invalid query: ' . $this->conn->error);
            }

            return $result == true ? 1 : 0;
        }

        public function countCorrectAnswer(int $user_id) {
            $sql = "SELECT * FROM tbl_quiz WHERE is_correct = 1 AND user_id = '$user_id'";
            $result = $this->conn->query($sql);

            $row_count = $result->num_rows;

            return $row_count;
        }

        public function countQuestion(int $user_id, int $question_id) {
            $sql = "SELECT * FROM tbl_quiz WHERE user_id = '$user_id' AND question_id = '$question_id'";
            $result = $this->conn->query($sql);

            $row_count = $result->num_rows;

            return $row_count;
        }

        public function updateAnswer(Quiz $quiz) {
            $sql = "UPDATE tbl_quiz SET option_id = $quiz->option_id WHERE question_id = '$quiz->question_id'";
            $result = $this->conn->query($sql);

            if (!$result) {
                trigger_error('Invalid query: ' . $this->conn->error);
            }

            return $result == true ? 1 : 0;
        }
    }