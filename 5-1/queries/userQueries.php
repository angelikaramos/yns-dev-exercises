<?php

    include_once '../classes/user.php';

    interface IUserQueries {
        public function countUsersByUsername(string $username);
        public function insertUser(User $user);
        public function checkPassword(User $user);
        public function getUserByUsername(string $username);
    }

    class UserQueries implements IUserQueries {
        private $conn;

        public function __construct($connection) {
            $this->conn = $connection;
        }

        public function countUsersByUsername(string $username) {
            $sql = "SELECT * FROM tbl_users WHERE username = '$username'";
            $result = $this->conn->query($sql);

            $row_count = $result->num_rows;

            return $row_count;
        }

        public function insertUser(User $user) {
            $sql = "INSERT INTO tbl_users (email,username,password) VALUES ('$user->email','$user->username',
            '$user->password')";
            $result = $this->conn->query($sql);

            if (!$result) {
                trigger_error('Invalid query: ' . $this->conn->error);
            }

            return $result == true ? 1 : 0;
        }

        public function checkPassword(User $user) {
            $sql = "SELECT * FROM tbl_users WHERE username = '$user->username'";
            $result = $this->conn->query($sql);
            $row = $result->fetch_assoc();

            $check = password_verify($user->password, $row['password']);

            return $check == true ? 1 : 0;
        }

        public function getUserByUsername(string $username) {
            $sql = "SELECT * FROM tbl_users WHERE username = '$username'";
            $result = $this->conn->query($sql);

            $users = array();
            while ($row = $result->fetch_assoc()) {
                $user = new User();

                $user->id = $row['id'];
                $user->email = $row['email'];
                $user->username = $row['username'];
                $user->password = $row['password'];

                array_push($users, $user);
            }

            return $users;
        }
    }