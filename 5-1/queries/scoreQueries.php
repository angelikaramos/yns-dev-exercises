<?php

    include_once '../classes/score.php';

    interface IScoreQueries {
        public function insertScore(int $user_id, int $score);
        public function getScores(int $user_id);
    }

    class ScoreQueries implements IScoreQueries {
        private $conn;

        public function __construct($connection) {
            $this->conn = $connection;
        }

        public function insertScore(int $user_id, int $score) {
            $sql = "INSERT INTO tbl_scores (user_id, score) VALUES ('$user_id', '$score')";
            $result = $this->conn->query($sql);

            if (!$result) {
                trigger_error('Invalid query' . $this->conn->error);
            }

            return $result == true ? 1 : 0;
        }

        public function getScores(int $user_id) {
            $sql = "SELECT * FROM tbl_scores where user_id = '$user_id'";
            $result = $this->conn->query($sql);

            $scores = array();
            while ($row = $result->fetch_assoc()) {
                $score = new Score();

                $score->id = $row['id'];
                $score->score = $row['score'];
                $score->date = $row['date'];

                array_push($scores, $score);
            }

            return $scores;
        }
    }