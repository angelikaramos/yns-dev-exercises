<?php

    include_once '../classes/answer.php';

    interface IAnswerQueries {
        public function getAnswerIdByQuestionId(int $question_id);
        public function getAnswerTextByQuestionId(int $question_id);
    }

    class AnswerQueries implements IAnswerQueries {
        private $conn;

        public function __construct($connection) {
            $this->conn = $connection;
        }

        public function getAnswerIdByQuestionId(int $question_id) {
            $sql = "SELECT * FROM tbl_answers WHERE question_id = '$question_id'";
            $result = $this->conn->query($sql);
            $row = $result->fetch_assoc();

            $answer = new Answer();
            $answer->id = $row['id'];

            return $answer;
        }

        public function getAnswerTextByQuestionId(int $question_id) {
            $sql = "SELECT * FROM tbl_answers WHERE question_id = '$question_id'";
            $result = $this->conn->query($sql);
            $row = $result->fetch_assoc();

            $answer = new Answer();
            $answer->answerText = $row['answer_text'];

            return $answer;
        }
    }