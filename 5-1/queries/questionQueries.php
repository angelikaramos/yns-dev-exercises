<?php

    include_once '../classes/question.php';

    interface IQuestionQueries {
        public function getQuestionById(int $question_id);
    }

    class QuestionQueries implements IQuestionQueries {
        private $conn;

        public function __construct($connection) {
            $this->conn = $connection;
        }

        public function getQuestionById(int $question_id) {
            $sql = "SELECT * FROM tbl_questions WHERE id = '$question_id'";
            $result = $this->conn->query($sql);

            $questions = array();
            while ($row = $result->fetch_assoc()) {
                $question = new Question();

                $question->id = $row['id'];
                $question->questionText = $row['question_text'];

                array_push($questions, $question);
            }

            return $questions;
        }
    }