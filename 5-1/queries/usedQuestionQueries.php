<?php

    include_once '../classes/usedQuestion.php';

    interface IUsedQuestionQueries {
        public function insertUsedQuestion(int $user_id, int $question_id);
        public function countUsedQuestions(int $user_id);
        public function truncateUsedQuestions(int $user_id);
    }

    class UsedQuestionQueries implements IUsedQuestionQueries {
        private $conn;

        public function __construct($connection) {
            $this->conn = $connection;
        }

        public function insertUsedQuestion(int $user_id,int $question_id) {
            $sql = "INSERT INTO tbl_used_questions (user_id,question_id) VALUES ('$user_id','$question_id')";
            $result = $this->conn->query($sql);

            if (!$result) {
                trigger_error('Invalid query: ' . $this->conn->error);
            }

            return $result == true ? 1 : 0;
        }

        public function countUsedQuestions(int $user_id) {
            $sql = "SELECT * FROM tbl_used_questions WHERE $user_id = '$user_id'";
            $result = $this->conn->query($sql);
            
            $row_count = $result->num_rows;

            return $row_count;
        }

        public function truncateUsedQuestions(int $user_id) {
            $sql = "DELETE from tbl_used_questions WHERE $user_id = '$user_id'";
            $result = $this->conn->query($sql);

            if (!$result) {
                trigger_error('Invalid query: ' . $this->conn->error);
            }

            return $result == true ? 1 : 0;
        }   
    }