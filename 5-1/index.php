<?php
    session_start();

    if (isset($_SESSION['id']) && isset($_SESSION['username'])) {
        header("Location: assets/user_index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>QUIZTIME!</title>
        <link rel="stylesheet" href="assets/css/styles.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
    </head>
    <body>
        <div class="login-container d-flex align-items-center justify-content-center">
            <form action="includes/login.inc.php" method="post" class="login-form text-center">
                <img src="assets/img/logo.png">
                <?php 
                if (isset($_SESSION['success_message'])) {
                    echo "<p class='text-success font-weight-bold'>" . $_SESSION['success_message'] . "</p>";
                    unset($_SESSION['success_message']);
                }
                if (isset($_SESSION['error_message'])) {
                    echo "<p class='text-danger font-weight-bold'>" . $_SESSION['error_message'] . "</p>";
                    unset($_SESSION['error_message']);
                }
                ?>
                <div class="form-group">
                    <input type="text" name="username" class="form-control rounded-pill form-control-lg" placeholder="Username">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control rounded-pill form-control-lg" placeholder="Password">
                </div>
                <p class="mt-3 font-weight-normal">
                    Don't have account yet?<a href="assets/register.php" class="text-success"><strong> Click here </strong></a>to register
                </p>
                <button type="submit" name="login" class="btn btn-success text-uppercase btn-block rounded-pill btn-lg">Login</button>
            </form>
        </div>
    </body>
</html>