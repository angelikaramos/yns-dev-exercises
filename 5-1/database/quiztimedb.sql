-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2020 at 11:00 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quiztimedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_answers`
--

CREATE TABLE `tbl_answers` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_text` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_answers`
--

INSERT INTO `tbl_answers` (`id`, `question_id`, `answer_text`) VALUES
(1, 1, 'Service Set Identifier'),
(2, 2, 'Personal-Area Network'),
(3, 3, 'James Gosling'),
(4, 4, 'submit'),
(5, 5, 'h1'),
(6, 6, '_blank'),
(7, 7, 'Advanced Research Project Agency Network'),
(8, 8, 'Forward packages and find the best path between networks'),
(9, 9, 'Local-Area Network'),
(10, 10, 'Wide-Area Network');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_options`
--

CREATE TABLE `tbl_options` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `option_text` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_options`
--

INSERT INTO `tbl_options` (`id`, `question_id`, `option_text`) VALUES
(1, 1, 'Set Service Identifier'),
(2, 1, 'Service Set Identifier'),
(3, 1, 'Set Server Identifier'),
(4, 2, 'Local-Area Network'),
(5, 2, 'Personal-Area Network'),
(6, 2, 'Metropolitan Area Network'),
(7, 3, 'Tim Berners-Lee'),
(8, 3, 'Bjarne Stroustrup'),
(9, 3, 'James Gosling'),
(10, 4, 'button'),
(11, 4, 'submit'),
(12, 4, 'radio'),
(13, 5, 'h1'),
(14, 5, 'h6'),
(15, 5, 'head'),
(16, 6, '_blank'),
(17, 6, '_next'),
(18, 6, '_new'),
(19, 7, 'Advanced Research Project Agenda Network'),
(20, 7, 'Agency Research Project Advanced Network'),
(21, 7, 'Advanced Research Project Agency Network'),
(22, 8, 'Filter the data stream, and control communications that go through the network'),
(23, 8, 'Connect computers that are within the same network and have many ports'),
(24, 8, 'Forward packages and find the best path between networks'),
(25, 9, 'Local-Area Network'),
(26, 9, 'Personal-Area Network'),
(27, 9, 'Wide-Area Network'),
(28, 10, 'Local-Area Network'),
(29, 10, 'Wide-Area Network'),
(30, 10, 'Personal-Area Network');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `question_text` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `question_text`) VALUES
(1, 'An SSID is a unique ID that consists of 32 characters and is used for naming wireless networks. SSID stands for'),
(2, 'The term used to describe the small networks where connected wireless devices are within personal reach.'),
(3, 'Java was started as a project called \"Oak\" in June 1991 by'),
(4, 'Which value for the type attribute turns the input tag into a submit button?'),
(5, 'Choose the correct HTML tag for the largest heading'),
(6, 'Which value of the target attribute makes the link open in a new tab or a new window?'),
(7, 'ARPANET stands for:'),
(8, 'What are the main functions of a router?'),
(9, 'The term used to describe networks in a small or local geographic area, such as a home, small business, or a department within a large corporation.'),
(10, 'The term used to describe a collection of LANs that provides inter-LAN and internet connectivity for networks over long distance.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quiz`
--

CREATE TABLE `tbl_quiz` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `is_correct` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_scores`
--

CREATE TABLE `tbl_scores` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `score` varchar(255) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_used_questions`
--

CREATE TABLE `tbl_used_questions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `email`, `username`, `password`) VALUES
(1, 'angelikaramos.yns@gmail.com', 'angel', '$2y$10$yJPm6HqpBQXpOmTDXfNqyuB.Ub2ZRqkpCFTDDOGm.vEd8kCqe0fi6'),
(2, 'kathrynbernardo@gmal.com', 'kathryn', '$2y$10$cGvCwMa0f6U0xsC28UOmfeo3cY8seTzRV4bnOt/DzXJPR4UdxxRq.'),
(3, 'lizahope@gmail.com', 'liza', '$2y$10$ZBzyAxsBlwVYC1oefoHfQO91uaHEyWX7QDAEvlrUG0izb6H5GMplm');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_answers`
--
ALTER TABLE `tbl_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_options`
--
ALTER TABLE `tbl_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_quiz`
--
ALTER TABLE `tbl_quiz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_scores`
--
ALTER TABLE `tbl_scores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_used_questions`
--
ALTER TABLE `tbl_used_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_answers`
--
ALTER TABLE `tbl_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_options`
--
ALTER TABLE `tbl_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_quiz`
--
ALTER TABLE `tbl_quiz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_scores`
--
ALTER TABLE `tbl_scores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_used_questions`
--
ALTER TABLE `tbl_used_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
