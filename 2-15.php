<!DOCTYPE html>
<html>
    <head>
        <title>Show current date and time in real time.</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
        <script>
            window.addEventListener('DOMContentLoaded', (event) => {
                // Get html element by id
                dateAndTime = document.getElementById("dateandtime");

                var today = new Date();
                var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                var dateTime = date+' '+time;

                // Passed the dateTime to label
                dateAndTime.innerHTML = dateTime;
            
                // Refresh every minute
                setInterval(function(){ 
                    location.reload(); 
                }, 60000);
            });
        </script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include 'includes/navbar.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                </div>
                <!-- Right side content -->
                <div class="col-lg-6 col-md-6 form-container">
                    <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
                        <!-- Logo -->
                        <div class="logo mb-3">
                            <img src="assets/img/yns.png" alt="yns_logo" width="150px">
                        </div>
                        <!-- Heading or Title of Exercise -->
                        <div class="heading mb-4">
                            <h4>Current Date and Time in Real Time</h4>
                        </div>
                        <label for="showDateTime">Date and Time: </label>
                        <label for="dateandtime" id="dateandtime"></label>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>