<?php

    if (isset($_POST['Submit'])) {
        // Data from form
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $email = $_POST['email'];
        $mobilenumber = $_POST['mobilenumber'];
        $userID = $_POST['userID'];
        $password = $_POST['password'];
        
        // Initialize variable
        $errors = array();
        $extensions = array("jpeg","jpg","png");
        $file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $file_tmp = $_FILES['image']['tmp_name'];
        $file_type = $_FILES['image']['type'];
        $tmp = explode('.',$file_name);
        $file_ext = strtolower(end($tmp));

        // Validations
        if (empty($firstname)) {
            $errors[] = "First name is required";
        } elseif (!preg_match("/^[\p{L} ]+$/u", $firstname)) {  
            $errors[] = "First Name: Only alphabets and whitespace are allowed.";  
        }  
        if (empty($lastname)) {
            $errors[] = "Last name is required";
        } elseif (!preg_match("/^[\p{L} ]+$/u", $lastname)) {  
            $errors[] = "Last Name: Only alphabets and whitespace are allowed.";  
        }
        if (empty($email)) {
            $errors[] = "Email is required";
        } elseif (!preg_match("/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/", $email) && isset($email)) {
            $errors[] = "Email: Must be a valid email";
        }
        if (empty($mobilenumber)) {
            $errors[] = "Mobile number is required";
        } elseif (!preg_match("/^([0-9]*)$/", $mobilenumber)) {
            $errors[] = "Mobile number: Must be a valid mobile number";
        } elseif (strlen($mobilenumber) != 11) {
            $errors[] = "Mobile number: Must be 11 digits";
        }
        if (empty($file_name)) {
            $errors[] = "Image is required";
        } elseif (in_array($file_ext,$extensions) === false) {
            $errors[] = "Image: Extension not allowed, please choose a JPEG, JPG OR PNG file.";
        }
        if (empty($userID)) {
            $errors[] = "UserID is required";
        }
        if (empty($password)) {
            $errors[] = "Password is required";
        } elseif (strlen($password) < 8) {
            $errors[] = "Password must be atleast 8 characters";
        }

        if (!empty($firstname) && !empty($lastname) && !empty($email) && !empty($mobilenumber) 
            && !empty($file_name) && !empty($userID) && !empty($password)) {
            // Data will append in CSV
            $data = "$firstname, $lastname, $email, $mobilenumber, $file_name, $userID, $password\n";
        
            // $filepointer is the file pointer to the file
            $filepointer = fopen("../csv/YNS-User-Information.csv", "a");

            if (isset($_FILES['image'])) {
                if ($file_size > 2097152) {
                    $errors[] = "Image: File size must be exactly 2 MB";
                }
                // If no errors upload image and add data in CSV
                if (empty($errors) == true) {
                    // Upload image in uploads folder
                    move_uploaded_file($file_tmp,"../uploads/".$file_name);
                
                    if ($filepointer) {
                        // Appending the data into CSV
                        if (fwrite($filepointer,$data)) {
                            $csv_msg = "Success";
                        }
                        // Close the file
                        fclose($filepointer);
                    }
                    if ($csv_msg == "Success") {
                        unset($errors);
                        $success_msg = "User Information Successfully Added!";
                    }
                } 
            }
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Upload images</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include '../includes/navbar_subfolders.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                    <span>
                        <?php
                            if (isset($errors)) {
                                echo "<div class='alert alert-warning alert-dismissible fade show' role='alert'>
                                <strong>Oh no!</strong> You should check in on some of these fields.<br>";
                                foreach($errors as $error) {
                                    echo "<br>*" . $error; 
                                }
                                echo "</div>";
                            }
                            if (isset($success_msg)) {
                                echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                                <strong>Awesome! </strong>";
                                echo $success_msg; 
                                echo "</div>";
                            }
                        ?>
                    </span>
                </div>
                <!-- Right side form -->
                <div class="col-lg-6 col-md-6 form-container">
                    <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
                        <!-- Logo -->
                        <div class="logo mb-3">
                            <img src="../assets/img/yns.png" alt="yns_logo" width="100px">
                        </div>
                        <!-- Heading or Title of Exercise -->
                        <div class="heading mb-4">
                            <h4>Upload images</h4>
                        </div>
                        <!-- Form -->
                        <form method="post" action="1-10.php" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-input">
                                        <label for="firstname">First Name:</label><br>
                                        <input type="text" name="firstname">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-input">
                                        <label for="lastname">Last Name:</label><br>
                                        <input type="text" name="lastname">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-input">
                                        <label for="email">Email:</label><br>
                                        <input type="text" name="email">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-input">
                                        <label for="mobilenumber">Mobile Number:</label><br>
                                        <input type="text" name="mobilenumber" maxlength="11">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-input">
                                        <label for="userID">User ID:</label><br>
                                        <input type="text" name="userID">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-input">
                                        <label for="password">Password:</label><br>
                                        <input type="password" name="password">
                                    </div>
                                </div>
                            </div>
                            <div class="form-input">
                                <label for="image">Upload an image:</label>
                                <input type="file" name="image" id="image">
                            </div>
                            <button type="submit" name="Submit">Submit</button><br><br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>