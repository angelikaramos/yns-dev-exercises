<!DOCTYPE html>
<html>
    <head>
        <title>Change text and background color when you press buttons.</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
        <script>
            window.addEventListener('DOMContentLoaded', (event) => {
                // Get html element by id
                var bgRed = document.getElementById('bgRed');
                var bgGreen = document.getElementById('bgGreen');
                var bgBlue = document.getElementById('bgBlue');
                var bgYellow = document.getElementById('bgYellow');
                var bgBlack = document.getElementById('bgBlack');
                var textRed = document.getElementById('textRed');
                var textGreen = document.getElementById('textGreen');
                var textBlue = document.getElementById('textBlue');
                var textYellow = document.getElementById('textYellow');
                var textBlack = document.getElementById('textBlack');
                var text = document.getElementById('welcomeText')

                // Change Background Color
                function changeBgColor(color) {
                    document.body.style.backgroundColor = color;
                }
                
                // Change Text Color
                function changeTxtColor(color) {
                    text.style.color = color;
                }
                
                // When Background Color buttons clicked
                bgRed.addEventListener('click', function() {changeBgColor('red')}, false);
                bgGreen.addEventListener('click', function() {changeBgColor('green')}, false);
                bgBlue.addEventListener('click', function() {changeBgColor('blue')}, false);
                bgYellow.addEventListener('click', function() {changeBgColor('yellow')}, false);
                bgBlack.addEventListener('click', function() {changeBgColor('black')}, false);

                // When Text Color buttons clicked
                textRed.addEventListener('click', function() {changeTxtColor('red')}, false);
                textGreen.addEventListener('click', function() {changeTxtColor('green')}, false);
                textBlue.addEventListener('click', function() {changeTxtColor('blue')}, false);
                textYellow.addEventListener('click', function() {changeTxtColor('yellow')}, false);
                textWhite.addEventListener('click', function() {changeTxtColor('white')}, false);
            });
        </script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include 'includes/navbar.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                </div>
                <!-- Right side form -->
                <div class="col-lg-6 col-md-6 form-container">
                    <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
                        <!-- Logo -->
                        <div class="logo mb-3">
                            <img src="assets/img/yns.png" alt="yns_logo" width="150px">
                        </div>
                        <!-- Heading or Title of Exercise -->
                        <div class="heading mb-4">
                            <h4>Change Text and Background Color</h4>
                        </div>
                        <!-- Buttons -->
                        <div class="form-input">
                            <h2>Background Color:</h2>
                            <button type="submit" id="bgRed">Red</button>
                            <button type="submit" id="bgGreen">Green</button>
                            <button type="submit" id="bgBlue">Blue</button>
                            <button type="submit" id="bgYellow">Yellow</button>
                            <button type="submit" id="bgBlack">Black</button><br>
                            <h2>Text Color:</h2>
                            <button type="submit" id="textRed">Red</button>
                            <button type="submit" id="textGreen">Green</button>
                            <button type="submit" id="textBlue">Blue</button>
                            <button type="submit" id="textYellow">Yellow</button>
                            <button type="submit" id="textWhite">White</button><br><br><br>
                            <h1 id="welcomeText">Welcome!</h1>
                        </div>        
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>