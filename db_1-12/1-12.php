<?php

    require_once '../databases/dbConnect.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Add pagination in the list page</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include '../includes/navbar_subfolders.inc.php'; ?>
        <div class="bs-example">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th colspan="8">User Information</th>
                    </tr>
                    <tr>
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Mobile Number</th>
                        <th>UserID</th>
                        <th>Profile Picture</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $from = 0;
                        if (isset($_GET['page'])) {
                            $page = $_GET['page'];
                            // Select on the specific row
                            $from = ($page*10)-10;
                        }
                        $sql = "SELECT * FROM tbl_users LIMIT $from ,10";
                        $result = mysqli_query($conn, $sql);
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo "<tr>";
                                echo "<td>" . $row['id'] . "</td>";
                                echo "<td>" . $row['first_name'] . "</td>";
                                echo "<td>" . $row['last_name'] . "</td>";
                                echo "<td>" . $row['email'] . "</td>";
                                echo "<td>" . $row['mobile_number'] . "</td>";
                                echo "<td>" . $row['user_id']. "</td>";
                                if ($row['profile_picture'] == null) {
                                    echo "<td>No uploaded profile picture</td>";
                                } elseif (!empty($row['profile_picture'])) {
                                    echo "<td> 
                                        <img src='../uploads/" . $row['profile_picture'] . "' 
                                        style='height:150px;width:150px'>";
                                    echo "</td>";
                                }
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="pagination">
            <?php
                // Count total number of rows for pagination
                $sql = "SELECT * FROM tbl_users";
                $result = mysqli_query($conn, $sql);
                $row = mysqli_num_rows($result);
                $pages = ceil($row/10);
                for($i = 1; $i <= $pages; $i++) {
                    echo "<a href='1-12.php?page=" .$i . "'>" . $i . " </a>";
                }
            ?>
        </div>
    </body>
</html>