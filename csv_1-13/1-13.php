<?php

    session_start();

    // User can go directly to home page as long as this session exists
    if (isset($_SESSION["userID"]) || isset($_SESSION["password"])) {
        header("Location: 1-13_2.php");
    }

    if(isset($_POST['Submit'])){
        // Data from form
        $userID = $_POST['userID'];
        $password = $_POST['password'];

        // Initialize variable
        $row = 1;
        $errors = array();
        $userIDs = array();
        $userPass = array();

        // Validations
        if (empty($userID)) {
            $errors[] = "User ID is required";
        }
        if (empty($password)) {
            $errors[] = "Password is required";
        }
        
        if (!empty($userID) && !empty($password)) {
            // Check if existing in CSV
            if (($handle = fopen("../csv/YNS-User-Information.csv", "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    // String list of User ID and Password
                    $userIDs = $data[5];
                    $userPass = $data[6];

                    // Check if the inputs are valid
                    if (preg_match("/\b".$password."\b/i", $userPass) && 
                        preg_match("/\b".$userID."\b/i", $userIDs)) {
                        $_SESSION["userID"] = $userID;
                        $_SESSION["password"] = $password;
                        header("Location: 1-13_2.php");
                    } else {
                        $errors[] = "Not match";
                    }
                    $row++;
                }
                if (!empty($errors)) {
                    unset($errors);
                    $errors[] = "User ID and/or Password are invalid.";
                }
                fclose($handle);
            }
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Create login form and embed it into the system that you developed</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include '../includes/navbar_subfolders.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                    <span>
                        <?php
                            if (isset($errors)) {
                                echo "<div class='alert alert-warning alert-dismissible fade show' role='alert'>
                                <strong>Oh no!</strong> You should check in on some of these fields.<br>";
                                foreach($errors as $error) {
                                    echo "<br>*" . $error; 
                                }
                                echo "</div>";
                            }
                            if (isset($success_msg)) {
                                echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                                <strong>Awesome! </strong>";
                                echo $success_msg; 
                                echo "</div>";
                            }
                        ?>
                    </span>
                </div>
                <!-- Right side form -->
                <div class="col-lg-6 col-md-6 form-container">
                    <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
                        <!-- Logo -->
                        <div class="logo mb-3">
                            <img src="../assets/img/yns.png" alt="yns_logo" width="100px">
                        </div>
                        <!-- Heading or Title of Exercise -->
                        <div class="heading mb-4">
                            <h4>Login</h4>
                        </div>
                        <!-- Form -->
                        <form method="post" action="1-13.php">
                            <div class="form-input">
                                <label for="userID">User ID:</label><br>
                                <input type="text" name="userID"><br><br>
                            </div>
                            <div class="form-input">
                                <label for="password">Password:</label><br>
                                <input type="password" name="password"><br><br>
                            </div>
                            
                            <label for="registration">Not yet registered?</label>
                            <a href="1-10.php">Click here</a><br><br>
                            <button type="submit" name="Submit">Submit</button><br><br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>