<?php

    session_start();

    // User cannot go to this page unless they login
    if (empty($_SESSION["userID"]) || empty($_SESSION["password"])) {
        header("Location: 1-13.php");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Home Page</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include '../includes/navbar_subfolders.inc.php'; ?>
        <h1>Welcome to Home Page!</h1>
        <a href="logout.php"><button class="btn btn-primary" name="logout">Logout</button></a>
        <div class="bs-example">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th colspan="6">User Information</th>
                    </tr>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Mobile Number</th>
                        <th>UserID</th>
                        <th>Profile Picture</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $csv = array();
                        if (($csvfile = fopen("../csv/YNS-User-Information.csv", "r")) !== FALSE) {
                            while (($csvdata = fgetcsv($csvfile, 1000, ",")) !== FALSE && $row = 10) {
                                $csv[] = $csvdata;
                            }
                            $page = 1;
                            if (isset($_GET['page'])) {
                                $page = $_GET['page'];
                            }
                            // Getting the value of specific row data that will retrieve
                            if ($page >= 2) {
                                $firstrow = ($page*10)-9;
                            } else {
                                $firstrow = ($page*11)-10;
                            }
                            
                            $records = array_slice($csv,$firstrow, 10); 
                            foreach($records as $record) {
                                echo "<tr>";    
                                    echo "<td>" . $record[0] . "</td>";
                                    echo "<td>" . $record[1] . "</td>";
                                    echo "<td>" . $record[2] . "</td>";
                                    echo "<td>" . $record[3] . "</td>";
                                    echo "<td>" . $record[5] . "</td>";
                                    if (ltrim($record[4]) == null) {
                                        echo "<td>No uploaded profile picture</td>";
                                    } elseif ($record[4] == "Profile Picture") {
                                        echo "<td>" . $record[4] . "</td>";
                                    } elseif (!empty($record[4])) {
                                        echo "<td><img src=../uploads/".ltrim($record[4])." 
                                        style=height:100px;width:100px;></td>";
                                    }
                                echo "</tr>";
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="pagination">
            <?php
                // Count total number of records
                $row = count($csv)-1;
                $pages = ceil($row/10);
                for($i = 1; $i <= $pages; $i++) {
                    echo "<a href='1-13_2.php?page=" .$i . "'>" . $i . " </a>";
                }
            ?>
        </div>
    </body>
</html>