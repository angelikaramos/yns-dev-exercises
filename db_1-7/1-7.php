<?php

    require_once '../databases/dbConnect.php';

    if (isset($_POST['Submit'])) {
        // Data from form
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $email = $_POST['email'];
        $mobilenumber = $_POST['mobilenumber'];
        $user_id = $_POST['user_id'];
        $password = $_POST['password'];
        
        // Initialize variable
        $errors = array();

        // Validations
        // Check if User ID existing in Database
        $sql = "SELECT * FROM tbl_users WHERE user_id = '$user_id'";
        $result = mysqli_query($conn, $sql);
        $count = mysqli_num_rows($result);
        
        if (empty($firstname)) {
            $errors[] = "First name is required";
        } elseif (!preg_match("/^[\p{L} ]+$/u", $firstname)) {  
            $errors[] = "First Name: Only alphabets and whitespace are allowed.";  
        }  
        if (empty($lastname)) {
            $errors[] = "Last name is required";
        } elseif (!preg_match("/^[\p{L} ]+$/u", $lastname)) {  
            $errors[] = "Last Name: Only alphabets and whitespace are allowed.";  
        }
        if (empty($email)) {
            $errors[] = "Email is required";
        } elseif (!preg_match("/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/", $email) && isset($email)) {
            $errors[] = "Email: Must be a valid email";
        }
        if (empty($mobilenumber)) {
            $errors[] = "Mobile number is required";
        } elseif (!preg_match("/^([0-9]*)$/", $mobilenumber)) {
            $errors[] = "Mobile number: Must be a valid mobile number";
        } elseif (strlen($mobilenumber) != 11) {
            $errors[] = "Mobile number: Must be 11 digits";
        }
        if (empty($user_id)) {
            $errors[] = "User ID is required";
        } elseif ($count > 0) {
            $errors[] = "User ID already exists";
        }
        if (empty($password)) {
            $errors[] = "Password is required";
        } elseif (strlen($password) < 8) {
            $errors[] = "Password must be atleast 8 characters";
        }

        if (empty($errors) && !empty($firstname) && !empty($lastname) && !empty($email) && !empty($mobilenumber) 
            && !empty($user_id) && !empty($password)) {
            // Password Hashing
            $password = password_hash($password, PASSWORD_DEFAULT);
            
            // Data will inserted in Database
            $sql = "INSERT INTO tbl_users (first_name, last_name, email, mobile_number, user_id, password) 
                    VALUES ('$firstname','$lastname','$email','$mobilenumber','$user_id',
                    '$password')";

            // Check if data successfully inserted
            if (mysqli_query($conn, $sql) === TRUE) {
                header("Location: 1-7_2.php");
                exit();
            }

            // If data inserted fails
            if (mysqli_query($conn, $sql) === FALSE) {
                echo "Something went wrong";
            }
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Add validation in the user information form(required, numeric, character, mailaddress).</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include '../includes/navbar_subfolders.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                    <span>
                        <?php
                            if (isset($errors)) {
                                echo " <div class='alert alert-warning alert-dismissible fade show' role='alert'>
                                <strong>Oh no!</strong> You should check in on some of these fields.<br>";
                                foreach($errors as $error) {
                                    echo "<br>*" . $error; 
                                }
                                echo "</div>";
                            }
                        ?>
                    </span>
                </div>
                <!-- Right side form -->
                <div class="col-lg-6 col-md-6 form-container">
                    <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
                        <!-- Logo -->
                        <div class="logo mb-3">
                            <img src="../assets/img/yns.png" alt="yns_logo" width="100px">
                        </div>
                        <!-- Heading or Title of Exercise -->
                        <div class="heading mb-4">
                            <h4>Form with Validations</h4>
                        </div>
                        <!-- Form -->
                        <form method="post" action="1-7.php">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-input">
                                        <label for="firstname">First Name:</label><br>
                                        <input type="text" name="firstname">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-input">
                                        <label for="lastname">Last Name:</label><br>
                                        <input type="text" name="lastname">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-input">
                                        <label for="email">Email:</label><br>
                                        <input type="text" name="email">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-input">
                                        <label for="mobilenumber">Mobile Number:</label><br>
                                        <input type="text" name="mobilenumber" maxlength="11">
                                    </div>
                                </div>
                            </div>
                            <div class="form-input">
                                <label for="userID">User ID:</label><br>
                                <input type="text" name="user_id">
                            </div>
                            <div class="form-input">
                                <label for="password">Password:</label><br>
                                <input type="password" name="password">
                            </div>
                            <button type="submit" name="Submit">Submit</button><br><br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>