<!DOCTYPE html>
<html>
    <head>
        <title>Input user information. Then show it in next page</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include '../includes/navbar_subfolders.inc.php'; ?>
        <div class="bs-example">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th colspan="6">User Information</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if (($csvfile = fopen("../csv/YNS-User-Information.csv", "r")) !== FALSE) {
                            while (($csvdata = fgetcsv($csvfile, 1000, ",")) !== FALSE) {
                                echo "<tr>";
                                    echo "<td>" . $csvdata[0] . "</td>";
                                    echo "<td>" . $csvdata[1] . "</td>";
                                    echo "<td>" . $csvdata[2] . "</td>";
                                    echo "<td>" . $csvdata[3] . "</td>";
                                    echo "<td>" . $csvdata[5] . "</td>";
                                    echo "<td>" . $csvdata[6] . "</td>";
                                echo "</tr>";
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </body>
</html>