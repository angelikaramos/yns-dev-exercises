<!DOCTYPE html>
<html>
    <head>
        <title>Show prime numbers</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
        <script>
            window.addEventListener('DOMContentLoaded', (event) => {
                // Get html element by id
                var getprime = document.getElementById('getprime');

                // When button clicked, get prime number function
                getprime.addEventListener('click', function() {
                    // Get value of the element by id
                    var lastnumber = document.getElementById('lastnumber').value;
                    var result = document.getElementById('primenumbers');
                    var primenumbers = " ";

                    function isPrime(num) {
                        for(var i = 2; i < num; i++) {
                            if (num % i == 0) {
                                return;
                            }
                        }
                        result.innerHTML = primenumbers += i + " ";
                    }
                    
                    // Check if the number is less than 2
                    if (lastnumber < 2) {
                        result.innerHTML = 'Enter atleast number 2 and up';
                    }
                    // Check if the number is greater than or equal to 2
                    if (lastnumber >= 2) {
                        // Check if the numbers are prime using function
                        for(var i = 2; i <= lastnumber; i++) {
                            isPrime(i);
                        }
                    }
                });
            });
        </script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include 'includes/navbar.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                </div>
                <!-- Right side form -->
                <div class="col-lg-6 col-md-6 form-container">
                    <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
                        <!-- Logo -->
                        <div class="logo mb-3">
                            <img src="assets/img/yns.png" alt="yns_logo" width="150px">
                        </div>
                        <!-- Heading or Title of Exercise -->
                        <div class="heading mb-4">
                            <h4>Prime Numbers</h4>
                        </div>
                        <!-- Form -->
                        <div class="form-input">
                            <label for="lastnumber">Last number:</label>
                            <input type="text" id="lastnumber">
                        </div>
                        <label for="result">Result:</label>
                        <p id="primenumbers"></p>     
                        <button type="submit" id="getprime">Submit</button>     
                    </div>
                </div>
            </div>
        </div> 
    </body>
</html>