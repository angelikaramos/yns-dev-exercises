<!DOCTYPE html>
<html>
    <head>
        <title>Change size of images when you press buttons.</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
        <script>
            window.addEventListener('DOMContentLoaded', (event) => {
                // Get html element by id
                var btnSmall = document.getElementById('btnSmall');
                var btnMedium = document.getElementById('btnMedium');
                var btnLarge = document.getElementById('btnLarge');
                var img = document.getElementById('img');

                // When button Small clicked - small image
                btnSmall.addEventListener('click', function() {
                    img.height = "60";
                    img.width = "50";
                });
                
                // When button Medium clicked - medium image
                btnMedium.addEventListener('click', function() {
                    img.height = "150";
                    img.width = "130";
                });

                // When button Large clicked - large image
                btnLarge.addEventListener('click', function() {
                    img.height = "280";
                    img.width = "250";
                });
            });
        </script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include 'includes/navbar.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                </div>
                <!-- Right side form -->
                <div class="col-lg-6 col-md-6 form-container">
                    <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
                        <!-- Logo -->
                        <div class="logo mb-3">
                            <img src="assets/img/yns.png" alt="yns_logo" width="150px">
                        </div>
                        <!-- Heading or Title of Exercise -->
                        <div class="heading mb-4">
                            <h4>Size of Images</h4>
                        </div>
                        <!-- Image -->
                        <div class="form-input">
                            <img src="assets/img/Phineas_and_Ferb.jpg" id="img" alt="Phineas_and_Ferb" height="150" width="130">
                            <br><br>
                            <button type="submit" id="btnSmall">Small</button>
                            <button type="submit" id="btnMedium">Medium</button>
                            <button type="submit" id="btnLarge">Large</button>
                        </div>        
                    </div>
                </div>
            </div>
        </div> 
    </body>
</html>