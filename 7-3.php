<?php

    $server = "localhost";
    $user = "root";
    $password = "";
    $database = "ynsdb";

    // Connecting to the Database
    $conn = mysqli_connect($server, $user, $password, $database);

    // Checking the connecttion
    if (!$conn) {
        echo "Database error: " . mysqli_connect_error();
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Using Case conditional statement</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include 'includes/navbar.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                    <?php echo "<div class='alert alert-warning alert-dismissible fade show' role='alert'>
                       Using Case conditional statement.</div>"; 
                    ?>
                </div>
                <!-- Right side content -->
                <div class="col-lg-6 col-md-6 form-container">
                    <!-- Table -->
                    <div class="bs-example">
                        <table class="table table-bordered small">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>First Name</th>
                                    <th>Middle Name</th>
                                    <th>Last Name</th>
                                    <th>Birthdate</th>
                                    <th>Position</th>
                                    <th>Hire Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $sql = "SELECT a.id, a.first_name, a.middle_name, a.last_name, a.birthdate, a.hire_date, 
                                            CASE 
                                                WHEN c.id = 1 THEN 'CEO'
                                                WHEN c.id = 2 THEN 'CTO'
                                                WHEN c.id = 3 THEN 'CFO'
                                                ELSE a.id
                                            END as position
                                            FROM employees a, employee_positions b, positions c
                                            WHERE a.id = b.employee_id AND b.position_id = c.id";
                                    $result = mysqli_query($conn, $sql);
                                    while ($row = mysqli_fetch_assoc($result)) {
                                        echo "<tr>";
                                            echo "<td>" . $row['id'] . "</td>";
                                            echo "<td>" . $row['first_name'] . "</td>";
                                            echo "<td>" . $row['middle_name'] . "</td>";
                                            echo "<td>" . $row['last_name'] . "</td>";
                                            echo "<td>" . $row['birthdate'] . "</td>";
                                            echo "<td>" . $row['position'] . "</td>";
                                            echo "<td>" . $row['hire_date'] . "</td>";
                                        echo "</tr>";
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>