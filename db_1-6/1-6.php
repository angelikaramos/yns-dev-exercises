<?php

    require_once '../databases/dbConnect.php';

    if (isset($_POST['Submit'])) {
        // Data from form
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $email = $_POST['email'];
        $mobilenumber = $_POST['mobilenumber'];
        $user_id = $_POST['user_id'];
        $password = $_POST['password'];

        if (!empty($firstname) && !empty($lastname) && !empty($email) && !empty($mobilenumber) 
            && !empty($user_id) && !empty($password)) {
            // Password Hashing
            $password = password_hash($password, PASSWORD_DEFAULT);
            
            // Data will inserted in Database
            $sql = "INSERT INTO tbl_users (first_name, last_name, email, mobile_number, user_id, password) 
                    VALUES ('$firstname','$lastname','$email','$mobilenumber','$user_id',
                    '$password')";

            // Check if data successfully inserted
            if (mysqli_query($conn, $sql) === TRUE) {
                header("Location: 1-6_2.php");
                exit();
            }

            // If data inserted fails
            if (mysqli_query($conn, $sql) === FALSE) {
                echo "Something went wrong";
            }
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Input user information. Then show it in next page</title>
       <!-- CSS only -->
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include '../includes/navbar_subfolders.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                </div>
                <!-- Right side form -->
                <div class="col-lg-6 col-md-6 form-container">
                    <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
                        <!-- Logo -->
                        <div class="logo mb-3">
                            <img src="../assets/img/yns.png" alt="yns_logo" width="100px">
                        </div>
                        <!-- Heading or Title of Exercise -->
                        <div class="heading mb-4">
                            <h4>User Information</h4>
                        </div>
                        <!-- Form -->
                        <form method="post" action="1-6.php">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-input">
                                        <label for="firstname">First Name:</label><br>
                                        <input type="text" name="firstname">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-input">
                                        <label for="lastname">Last Name:</label><br>
                                        <input type="text" name="lastname">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-input">
                                        <label for="email">Email:</label><br>
                                        <input type="text" name="email">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-input">
                                        <label for="mobilenumber">Mobile Number:</label><br>
                                        <input type="text" name="mobilenumber" maxlength="11">
                                    </div>
                                </div>
                            </div>
                            <div class="form-input">
                                <label for="userID">User ID:</label><br>
                                <input type="text" name="user_id">
                            </div>
                            <div class="form-input">
                                <label for="password">Password:</label><br>
                                <input type="password" name="password">
                            </div>
                            <button type="submit" name="Submit">Submit</button><br><br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>