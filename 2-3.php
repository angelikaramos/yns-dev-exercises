<!DOCTYPE html>
<html>
    <head>
        <title>The four basic operations of arithmetic</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
        <script>
            window.addEventListener('DOMContentLoaded', (event) => {
                // Get html element by id
                var calculate = document.getElementById('calculate');

                // When button clicked, math operations function
                calculate.addEventListener('click', function() {
                    // Get html element by ID
                    var firstnumber = document.getElementById('firstnumber').value;
                    var secondnumber = document.getElementById('secondnumber').value;
                    var operations = document.getElementById('operations');
                    var resultlabel = document.getElementById('result');

                    // Convert string to number
                    var firstnumber = Number(firstnumber);
                    var secondnumber = Number(secondnumber);

                    // Get the value of selected operation
                    var operation = operations.options[operations.selectedIndex].value;
                    
                    // Calculation
                    if (operation === "Add") {
                        var result =  firstnumber + secondnumber;
                    }
                    if (operation === "Subtract") {
                        var result = firstnumber - secondnumber;
                    }
                    if (operation === "Multiply") {
                        var result = firstnumber * secondnumber;
                    }
                    if (operation === "Division") {
                        var result = firstnumber / secondnumber;
                    }

                    // Result
                    resultlabel.innerHTML = 'Result: ' + result;
                });
            });
        </script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include 'includes/navbar.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                </div>
                <!-- Right side form -->
                <div class="col-lg-6 col-md-6 form-container">
                    <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
                        <!-- Logo -->
                        <div class="logo mb-3">
                            <img src="assets/img/yns.png" alt="yns_logo" width="150px">
                        </div>
                        <!-- Heading or Title of Exercise -->
                        <div class="heading mb-4">
                            <h4>Arithmetic Operations</h4>
                        </div>
                        <!-- Form -->
                        <div class="form-input">
                            <label for="firstnumber">First Number:</label><br>
                            <input type="text" id="firstnumber" required>
                        </div>
                        <div class="form-input">
                            <label for="secondnumber">Second Number:</label><br>
                            <input type="text" id="secondnumber" required>
                        </div>
                        <div class="form-input">
                            <select id="operations"  class="form-control">
                                <option value="Add" selected>Addition</option>
                                <option value="Subtract">Subtraction</option>
                                <option value="Multiply">Multiplication</option>
                                <option value="Division">Division</option>
                            </select>
                        </div><br>
                        <label for="result" id="result"></label><br>
                        <button type="submit" id="calculate">Submit</button><br><br>                
                    </div>
                </div>
            </div>
        </div>   
    </body>
</html>