<?php

    $server = "localhost";
    $user = "root";
    $password = "";
    $database = "dailyworkshiftsdb";

    // Connecting to the Database
    $conn = mysqli_connect($server, $user, $password, $database);
    
    // Check the connection
    if (!$conn) {
        echo "Database error: " . mysqli_connect_error();
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>List all therapists according to their daily work shifts</title>
        <!-- CSS only -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" defer></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
    </head>
    <body>
        <!-- Navigation Bar -->
        <?php include 'includes/navbar.inc.php'; ?>
        <div class="container-fluid">
            <div class="row"> 
                <!-- Left side background -->
                <div class="col-lg-6 col-md-6 d-none d-md-block image-container">
                    <?php echo "<div class='alert alert-warning alert-dismissible fade show' role='alert'>
                       List all therapists according to their daily work shifts.</div>"; 
                    ?>
                </div>
                <!-- Right side content -->
                <div class="col-lg-6 col-md-6 form-container">
                    <!-- Table -->
                    <div class="bs-example">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Target Date</th>
                                    <th>Start Time</th>
                                    <th>End Time</th>
                                    <th>Sort Start Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $sql = "SELECT therapists.name, therapist_id, target_date, start_time, end_time,
                                            CASE 
                                                WHEN start_time  <= '05:59:59' AND start_time >= '00:00:00' 
                                                    THEN CONCAT(DATE_ADD(target_date, INTERVAL 1 DAY), ' ', start_time)
                                                ELSE CONCAT(target_date, ' ',start_time)
                                            END as sort_start_time
                                            FROM daily_work_shifts
                                            INNER JOIN therapists ON therapists.id = therapist_id
                                            ORDER BY target_date AND sort_start_time ASC";
                                    $result = mysqli_query($conn, $sql);
                                    while ($row = mysqli_fetch_assoc($result)) {
                                        echo "<tr>";
                                            echo "<td>" . $row['therapist_id'] . "</td>";
                                            echo "<td>" . $row['name'] . "</td>";
                                            echo "<td>" . $row['target_date'] . "</td>";
                                            echo "<td>" . $row['start_time'] . "</td>";
                                            echo "<td>" . $row['end_time'] . "</td>";
                                            echo "<td>" . $row['sort_start_time'] . "</td>";
                                        echo "</tr>";   
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>